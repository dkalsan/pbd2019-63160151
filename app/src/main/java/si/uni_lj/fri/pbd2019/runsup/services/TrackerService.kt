package si.uni_lj.fri.pbd2019.runsup.services

import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.*
import android.support.v4.app.ActivityCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.android.gms.location.*

import com.google.android.gms.tasks.Task
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities
import si.uni_lj.fri.pbd2019.runsup.model.GpsPoint
import si.uni_lj.fri.pbd2019.runsup.model.User
import si.uni_lj.fri.pbd2019.runsup.model.Workout
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper
import java.util.*

class TrackerService : Service() {

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder: Binder() {
        // Return this instance of LocalService so clients can call public methods
        fun getService(): TrackerService? {
            return this@TrackerService
        }
    }

    private var mBinder: IBinder? = LocalBinder()

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private var locationRequest: LocationRequest? = null
    private lateinit var broadcastHandler: Handler
    private lateinit var broadcastRunnable: Runnable
    private lateinit var autosaverHandler: Handler
    private lateinit var autosaverRunnable: Runnable
    private lateinit var databaseHelper: DatabaseHelper

    private lateinit var speedList: ArrayList<Float>
    private lateinit var workout: Workout

    private var prevLocation: Location? = null
    private var continueFlag = false
    private var state = Constants.STATE_STOPPED
    private var totalTime: Long = 0
    private var startTime: Long = 0
    private var pace: Double = 0.0
    private var sessionNumber: Long = 0L

    override fun onCreate() {
        super.onCreate()

        // Get the fused location provider
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Initialize location callback
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return

                /**
                 * NOTE: If the app is not in the foreground, we get the location update ONLY once per hour.
                 * We would have to use startForegroundService(), however this requires min api SDK 26
                 */

                resetAutosaverTimer()
                val location = locationResult.lastLocation
                updateLocationSpeedDistance(location)
            }
        }

        // Set a listener for user login only if no user logged in before during the workout
        val sharedPreferences = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
        sharedPreferences.registerOnSharedPreferenceChangeListener { _, key ->
            if(::workout.isInitialized && key == "userId" && (workout.user == null)) {
                workout.user = getCurrentUser()
            }
        }

        // Initialize speed list
        speedList = arrayListOf()

        // Initialize DatabaseHelper
        databaseHelper = DatabaseHelper(applicationContext)

        // Initialize location request
        createLocationRequest()

        // Initialize broadcaster runnable
        createBroadcastRunnable()

        // Initialize autosaver runnable
        createAutosaverRunnable()

        // Initialize binder
        mBinder = LocalBinder()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        when(intent?.action) {

            Constants.COMMAND_START -> {
                startTime = SystemClock.uptimeMillis()

                if(!::workout.isInitialized) {
                    workout = createNewWorkout(intent.getIntExtra("sportActivity", 0))
                } else {
                    changeWorkoutStatus(Workout.statusUnknown)
                }

                state = Constants.STATE_RUNNING

                startLocationUpdates()
                startBroadcasting()
                startAutosaverRunnable()
                Log.d("RunsUp!", "Picked COMMAND_START")
            }
            Constants.COMMAND_CONTINUE -> {
                startTime = SystemClock.uptimeMillis()

                if(!::workout.isInitialized) {
                    workout = createNewWorkout(intent.getIntExtra("sportActivity", 0))
                } else {
                    changeWorkoutStatus(Workout.statusUnknown)
                }

                state = Constants.STATE_CONTINUE
                continueFlag = true

                startLocationUpdates()
                startBroadcasting()
                startAutosaverRunnable()
                Log.d("RunsUp!", "Picked COMMAND_CONTINUE")
            }
            Constants.COMMAND_PAUSE -> {
                totalTime += SystemClock.uptimeMillis() - startTime

                if(::workout.isInitialized) {
                    changeWorkoutStatus(Workout.statusPaused)
                }
                state = Constants.STATE_PAUSED
                sessionNumber += 1

                stopLocationUpdates()
                stopBroadcasting()
                stopAutosaverRunnable()
                storeWorkoutToDb(true)
                Log.d("RunsUp!", "Picked COMMAND_PAUSE")
            }
            Constants.COMMAND_STOP -> {

                if(::workout.isInitialized) {
                    changeWorkoutStatus(Workout.statusEnded)
                }
                state = Constants.STATE_STOPPED

                stopLocationUpdates()
                stopBroadcasting()
                stopAutosaverRunnable()
                storeWorkoutToDb(true)
                // TODO: Stop service or check if the state is stopped when creating new workout
                Log.d("RunsUp!", "Picked COMMAND_STOP")
            }
            Constants.UPDATE_SPORT_ACTIVITY -> {
                // Update sportActivity in workout with workoutId
                Log.d("RunsUp!", "Received UPDATE COMMAND")

                val reqSportActivity = intent.getIntExtra("sportActivity", 0)

                if(workout.sportActivity != reqSportActivity) {
                    workout.sportActivity = reqSportActivity
                    storeWorkoutToDb(false)
                }

            }
            Constants.LOAD_WORKOUT_BY_ID -> {

                val qWorkoutId = intent.getLongExtra("workoutId", -1)

                // Check if the current running workout is the one requested
                if(!::workout.isInitialized || (::workout.isInitialized && (workout.id != qWorkoutId))) {
                    //Log.d("RunsUp!", "Loading the requested workout with id $qWorkoutId")

                    val workoutDao = databaseHelper.workoutDao()
                    workout = workoutDao.queryForId(qWorkoutId)

                    // Extract and set local values
                    totalTime = (workout.duration * 1e3).toLong()
                    pace = 0.0
                    sessionNumber = extractLastSessionNumber(workout)
                    //val positionList  = extractPositionList(workout)
                    speedList = extractSpeedList(workout)

                    speedList.forEachIndexed { ix, speed ->
                        Log.d("DEBUG", "$ix: $speed")
                    }

                    when(workout.status) {
                        Workout.statusUnknown -> {
                            startTime = SystemClock.uptimeMillis()
                            state = Constants.STATE_RUNNING
                            startLocationUpdates()
                            startBroadcasting()
                        }
                        Workout.statusPaused -> {
                            state = Constants.STATE_PAUSED
                            sessionNumber += 1
                        }
                    }
                } else {
                    Log.d("RunsUp!", "Service with this ID already running")
                }

            }
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    override fun onDestroy() {
        stopLocationUpdates()

        super.onDestroy()
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()?.apply {
            interval = 3000
            fastestInterval = 3000
            smallestDisplacement = 10f
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }

        val builder = locationRequest?.let {
            LocationSettingsRequest
                .Builder()
                .addLocationRequest(it)
        }

        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder?.build())

        task.addOnSuccessListener {
            Log.d("RunsUp!", "All location settings are satisfied.")
        }

        task.addOnFailureListener {
            Log.d("RunsUp!", "Location settings are not satisfied.")
        }

    }

    private fun startLocationUpdates() {
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun createBroadcastRunnable() {
        broadcastHandler = Handler()
        broadcastRunnable = object : Runnable {
            override fun run() {
                try {

                    // Something to fix a looper error
                    if(Looper.myLooper() == null) {
                        Looper.prepare()
                    }

                    // Calculate calories
                    if(speedList.size >= 2) {
                        workout.totalCalories = SportActivities.countCalories(
                            workout.sportActivity,
                            getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE).getFloat("weight", SportActivities.DEFAULT_WEIGHT),
                            speedList,
                            (totalTime + SystemClock.uptimeMillis() - startTime)/(1e3*3600)
                        )
                    }

                    // Calculate pace
                    if(speedList.size > 0 && prevLocation != null && (SystemClock.elapsedRealtimeNanos() - prevLocation!!.elapsedRealtimeNanos < 6e9)) {
                        pace = (1.0 / speedList.last()) * (1000.0/60)
                    } else {
                        pace = 0.0
                    }

                    if(speedList.size > 0 && prevLocation != null) {
                        if(SystemClock.elapsedRealtimeNanos() - prevLocation!!.elapsedRealtimeNanos < 6e9) {
                            pace = (1.0 / speedList.last()) * (1000.0/60)
                        } else {
                            pace = 0.0
                        }

                        workout.paceCounter += 1
                        workout.paceAvg = workout.paceAvg + (pace - workout.paceAvg) / (workout.paceCounter)
                    } else {
                        pace = 0.0
                    }

                    // Broadcast details
                    broadcastDetails()
                }
                finally {
                    broadcastHandler.postDelayed(this, 1000)
                }
            }
        }
    }

    private fun createAutosaverRunnable() {
        autosaverHandler = Handler()
        autosaverRunnable = object : Runnable {
            override fun run() {
                try{
                    storeWorkoutToDb(false)
                } finally {
                    autosaverHandler.postDelayed(this, 10000)
                }
            }
        }
    }

    private fun resetAutosaverTimer() {
        autosaverHandler.removeCallbacks(autosaverRunnable)
        autosaverHandler.postDelayed(autosaverRunnable, 10000)
    }

    private fun startAutosaverRunnable() {
        autosaverRunnable.run()
    }

    private fun stopAutosaverRunnable() {
        autosaverHandler.removeCallbacks(autosaverRunnable)
    }

    private fun startBroadcasting() {
        broadcastRunnable.run()
    }

    private fun stopBroadcasting() {
        broadcastHandler.removeCallbacks(broadcastRunnable)
    }

    private fun updateLocationSpeedDistance(currLocation: Location) {

        var currSpeed = 0.0f

        if(prevLocation != null) {
            currSpeed = currLocation.distanceTo(prevLocation) / ((currLocation.elapsedRealtimeNanos - prevLocation!!.elapsedRealtimeNanos)/1e9f)

            if(currSpeed.isFinite()) {
                speedList.add(currSpeed)
            }

            if(state != Constants.STATE_CONTINUE) {
                workout.distance += currLocation.distanceTo(prevLocation)
            } else {
                if (!continueFlag || (continueFlag && currLocation.distanceTo(prevLocation) <= 100)) {
                    workout.distance += currLocation.distanceTo(prevLocation)
                }
                continueFlag = false
            }

        }

        // Store location to the database
        if(prevLocation != null && currSpeed.isFinite()) {
            storeLocationToDb(currLocation, currSpeed)
        }

        prevLocation = currLocation

        // Store workout to the database
        storeWorkoutToDb(false)

    }

    private fun broadcastDetails() {
        val intent = Intent(Constants.BROADCAST_TRACKER_DETAILS).apply {
            putExtra("duration", ((totalTime + SystemClock.uptimeMillis() - startTime) / 1e3).toLong())
            putExtra("distance", workout.distance)
            putExtra("pace", pace)
            putExtra("calories", workout.totalCalories)
            putExtra("state", state)
            putExtra("sportActivity", workout.sportActivity)
            putExtra("location", prevLocation)
            putExtra("workoutId", workout.id)
            // putExtra("positionList", positionList)
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun createNewWorkout(selectedSportActivity: Int): Workout {

        val workoutDao = databaseHelper.workoutDao()

        // This horror is here because ORMLite behaves super weirdly and that's the only thing that works...
        // I'm super dissatisfied with this database
        val newWorkout = Workout(null, selectedSportActivity)
        workoutDao.create(newWorkout)
        newWorkout.user = getCurrentUser()
        newWorkout.title = "Workout ${newWorkout.id}"
        workoutDao.update(newWorkout)

        return newWorkout

    }

    private fun getCurrentUser(): User? {
        val userDao = databaseHelper.userDao()

        val userId = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .getLong("userId", -1L)

        return userDao.queryForId(userId)
    }

    private fun changeWorkoutStatus(status: Int) {
        val workoutDao = databaseHelper.workoutDao()

        workout.status = status
        workoutDao.update(workout)
    }

    private fun storeLocationToDb(location: Location, currSpeed: Float) {
        val gpsPointDao = databaseHelper.gpsPointDao()

        val gpsPoint = GpsPoint(
            workout,
            sessionNumber,
            location,
            (getDuration() / 1e3).toLong(),
            currSpeed,
            pace,
            workout.totalCalories
        )
        gpsPointDao.create(gpsPoint)
    }

    private fun extractLastSessionNumber(qWorkout: Workout): Long {
        val gpsPointDao = databaseHelper.gpsPointDao()

        val q = ArrayList(gpsPointDao.queryForEq("workout_id", qWorkout))

        return q.maxBy { it.sessionNumber }?.sessionNumber ?: 0

    }

    private fun extractSpeedList(qWorkout: Workout): ArrayList<Float> {
        val gpsPointDao = databaseHelper.gpsPointDao()

        val q = ArrayList(gpsPointDao.queryForEq("workout_id", qWorkout))

        return ArrayList(q.map { it.speed })
    }
    /*
    private fun extractLocationList(qWorkout: Workout): ArrayList<Location> {
        val gpsPointDao = databaseHelper.gpsPointDao()

        val q = ArrayList(gpsPointDao.queryForEq("workout_id", qWorkout))

        return ArrayList(q.map { gpsPoint ->
            Location(LocationManager.GPS_PROVIDER).apply {
                latitude = gpsPoint.latitude
                longitude = gpsPoint.longitude
            }
        })
    }

    private fun extractSpeedList(locations: ArrayList<Location>): ArrayList<Float> {
        val extractedSpeedList = ArrayList(locations.zipWithNext {
                prev, curr -> prev.distanceTo(curr) / ((curr.elapsedRealtimeNanos - prev.elapsedRealtimeNanos)/1e9f)
        })
    }
    */

    private fun storeWorkoutToDb(stoppedState: Boolean) {
        val workoutDao = databaseHelper.workoutDao()

        if(!stoppedState) {
            workout.duration = (getDuration() / 1e3).toLong()
        } else {
            workout.duration = (totalTime / 1e3).toLong()
        }
        workoutDao.update(workout)
    }

    // Public functions for testers
    fun getState(): Int {
        return state
    }

    // in milliseconds
    fun getDuration(): Long {
        return (totalTime + SystemClock.uptimeMillis() - startTime)
    }

    fun getDistance(): Double {
        if(::workout.isInitialized) {
            return workout.distance
        }
        return 0.0
    }

    fun getPace(): Double {
        return pace
    }
}