package si.uni_lj.fri.pbd2019.runsup.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.adapter_history.view.*
import kotlinx.android.synthetic.main.fragment_history.view.*
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.HistoryListAdapter
import si.uni_lj.fri.pbd2019.runsup.R
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity
import si.uni_lj.fri.pbd2019.runsup.model.Workout
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper

class HistoryFragment: Fragment() {

    private lateinit var databaseHelper: DatabaseHelper
    private lateinit var workout: Workout
    private lateinit var prefs: SharedPreferences
    private lateinit var prefsListener: SharedPreferences.OnSharedPreferenceChangeListener

    val RC_IMAGE = 210

    companion object {
        fun newInstance(): HistoryFragment {
            return HistoryFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        // Initialize db helper
        databaseHelper = DatabaseHelper(activity)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_history, container, false)

        activity.title = getString(R.string.menu_history)

        // Set on item click listener
        view.listview_history_workouts.setOnItemClickListener { parent, v, position, _ ->

            if(v.id == R.id.imageview_history_icon) {
                workout = parent.getItemAtPosition(position) as Workout
                Intent(Intent.ACTION_PICK).apply {
                    type = "image/*"
                    putExtra(Intent.EXTRA_MIME_TYPES, arrayListOf("image/jpeg", "image/png"))
                    startActivityForResult(this, RC_IMAGE)
                }
            } else {
                val workout: Workout = parent.getItemAtPosition(position) as Workout
                Intent(activity, WorkoutDetailActivity::class.java).apply {
                    putExtra("workoutId", workout.id)
                    activity.startActivity(this)
                }
            }

        }

        // Fetch the workout list and display it
        val userId = activity.getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .getLong(Constants.SAVED_USERID_KEY, -1)

        val workoutDao = databaseHelper.workoutDao()
        val userDao = databaseHelper.userDao()

        val user = userDao.queryForId(userId)

        if(user != null) {
            var qWorkout = workoutDao.queryForEq("user_id", user)
            qWorkout = ArrayList(qWorkout.filter { workout -> workout.status == Workout.statusEnded })
            qWorkout.sortByDescending { it.created }

            if(qWorkout.isNotEmpty()) {
                Log.d("RunsUp!", "Loding ${qWorkout.size} items")
                view.textview_history_noHistoryData.visibility = View.GONE
                view.listview_history_workouts.visibility = View.VISIBLE
                val adapter = HistoryListAdapter(activity, android.R.layout.simple_list_item_1, qWorkout)
                view.listview_history_workouts.adapter = adapter
            } else {
                view.textview_history_noHistoryData.visibility = View.VISIBLE
                view.listview_history_workouts.visibility = View.GONE
                Log.d("RunsUp!", "No items")
            }
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == RC_IMAGE) {

            val imgUri = data?.data

            // Load into the view
            Glide
                .with(activity)
                .load(imgUri)
                .asBitmap()
                .placeholder(R.drawable.ic_menu_camera)
                .into(view?.imageview_history_icon)

            // Save to database
            val workoutDao = databaseHelper.workoutDao()
            workout.imageUrl = imgUri.toString()
            workoutDao.update(workout)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.findItem(R.id.action_delete)?.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_clearhistory -> {

                val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
                builder.setTitle(R.string.dialog_title_history)
                builder.setMessage(R.string.dialog_message_history)
                builder.setPositiveButton(R.string.yes) { _, _ ->

                    val userDao = databaseHelper.userDao()
                    val workoutDao = databaseHelper.workoutDao()

                    val qUser = userDao
                        .queryForId(
                            activity.getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                                .getLong(Constants.SAVED_USERID_KEY, -1L)
                        )

                    if(qUser != null) {

                        val qWorkout = workoutDao.queryForEq("user_id", qUser)

                        if(qWorkout.isNotEmpty()) {

                            // Delete in database
                            qWorkout
                                .filter { it.status == Workout.statusEnded }
                                .forEach {
                                    it.status = Workout.statusDeleted
                                    workoutDao.update(it)
                                }

                            // Update views
                            val adapter = HistoryListAdapter(activity, android.R.layout.simple_list_item_1, arrayListOf())
                            view?.listview_history_workouts?.adapter = adapter
                            view?.listview_history_workouts?.visibility = View.GONE
                            view?.textview_history_noHistoryData?.visibility = View.VISIBLE
                        }
                    }
                }
                builder.setNegativeButton(R.string.no, null)
                builder.create().show()

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}