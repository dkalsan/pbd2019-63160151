package si.uni_lj.fri.pbd2019.runsup

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import si.uni_lj.fri.pbd2019.runsup.fragments.AboutFragment
import si.uni_lj.fri.pbd2019.runsup.fragments.HistoryFragment
import si.uni_lj.fri.pbd2019.runsup.fragments.StopwatchFragment
import si.uni_lj.fri.pbd2019.runsup.fragments.TrackSelectionFragment
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val TAG = "RunsUp!"

    private lateinit var auth: FirebaseAuth
    private lateinit var databaseHelper: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        // Set the StopwatchFragment if this is the first time the activity opened
        if(savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment_main_content, StopwatchFragment.newInstance(), "STOPWATCH")
                .commitNow()
        }

        // Set onClick listeners
        setOnClickListeners()

        // Set a listener for Firebase Auth changes
        auth = FirebaseAuth.getInstance()
        auth.addAuthStateListener {
            Log.d(TAG, "AuthStateListener callback called")

            // Update the main menu with the current user information
            updateNavigationHeaderUI(auth.currentUser)

            // Rebuild options menu according to the login status
            invalidateOptionsMenu()
        }

        databaseHelper = DatabaseHelper(this)

        getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .registerOnSharedPreferenceChangeListener { sharedPreferences, key ->
                invalidateOptionsMenu()
            }

    }

    override fun onResume() {
        super.onResume()

        // Refresh fragments in case the preferences changed
        if(supportFragmentManager.findFragmentByTag("HISTORY") != null) {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment_main_content, HistoryFragment.newInstance())
                .commitNow()
        }

        invalidateOptionsMenu()

    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)

        val userId = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .getLong(Constants.SAVED_USERID_KEY, -1L)

        menu.findItem(R.id.action_sync).isVisible = userId != -1L

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> {
                startSettingsActivity()
                return true
            }
            R.id.action_sync -> {
                // TODO: Sync the data
                Snackbar.make(drawer_layout, "This isn't the Sync you're looking for.", Snackbar.LENGTH_SHORT).show()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        Log.d(TAG, "Called onNavigationItemSelected")

        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_workout -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_main_content, StopwatchFragment.newInstance(), "STOPWATCH")
                    .commitNow()
            }
            R.id.nav_compete -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_main_content, TrackSelectionFragment.newInstance(), "COMPETE")
                    .commitNow()
            }
            R.id.nav_history -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_main_content, HistoryFragment.newInstance(), "HISTORY")
                    .commitNow()
            }
            R.id.nav_settings -> {
                startSettingsActivity()
                return false
            }
            R.id.nav_about -> {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_main_content, AboutFragment.newInstance(), "ABOUT")
                    .commitNow()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun updateNavigationHeaderUI(account: FirebaseUser?) {

        val header = nav_view.getHeaderView(0)

        val iv = header.findViewById<ImageView>(R.id.menu_loggedInUserImage)
        val tv = header.findViewById<TextView>(R.id.menu_loggedInUserFullName)

        if(account != null) {

            Glide
                .with(applicationContext)
                .load(account.photoUrl)
                .asBitmap()
                .placeholder(R.drawable.baseline_person_24)
                .override(180, 180)
                .into(iv)

            tv.text = account.displayName ?: getString(R.string.all_unknownuser)

        } else {

            iv.setImageDrawable(
                ContextCompat.getDrawable(
                    applicationContext,
                    R.drawable.baseline_person_24
                )
            )

            tv.text = getString(R.string.all_unknownuser)
        }
    }

    private fun setOnClickListeners() {
        val header = nav_view.getHeaderView(0)

        val handleHeaderButtonClick = View.OnClickListener {
            Intent(this, LoginActivity::class.java).also {
                startActivity(it)
            }
        }

        header.menu_loggedInUserImage.setOnClickListener(handleHeaderButtonClick)
        header.menu_loggedInUserFullName.setOnClickListener(handleHeaderButtonClick)
    }

    private fun startSettingsActivity() {
        Intent(this, SettingsActivity::class.java).also {
            startActivity(it)
        }
    }

    public override fun onPause() {
        super.onPause()
    }
}
