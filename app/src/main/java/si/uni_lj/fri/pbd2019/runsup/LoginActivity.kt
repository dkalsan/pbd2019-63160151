package si.uni_lj.fri.pbd2019.runsup

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_login.*
import si.uni_lj.fri.pbd2019.runsup.helpers.SportActivities
import si.uni_lj.fri.pbd2019.runsup.model.User
import si.uni_lj.fri.pbd2019.runsup.model.UserProfile
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper

class LoginActivity : AppCompatActivity() {

    private val TAG = "RunsUp!"

    private val RC_SIGN_IN = 420

    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var auth: FirebaseAuth
    private lateinit var databaseHelper: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_login_signin.setOnClickListener {
            val signInIntent = googleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        button_login_signout.setOnClickListener {

            // Sign out from Firebase
            FirebaseAuth.getInstance().signOut()

            // Sign out from Google
            googleSignInClient.signOut().addOnCompleteListener {
                updateSharedPreferences(null)
                updateUI(null)
            }
        }


        button_login_save.setOnClickListener {

            if(editText_login_weight.text.isNotEmpty()) {
                val user = auth.currentUser
                val weight = editText_login_weight.text.toString().toFloat()

                if(weight > 0.0) {
                    if(user != null) {
                        val userProfileDao = databaseHelper.userProfileDao()
                        val userProfile = getUserProfileFromDb(user.uid)
                        userProfile.weight = weight
                        userProfileDao.update(userProfile)
                    }

                    val sharedPref = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                    with (sharedPref.edit()) {
                        putFloat(Constants.SAVED_WEIGHT_KEY, weight)
                        putBoolean(Constants.SAVED_LOGINSTATE_KEY, true)
                        apply()
                    }
                } else {
                    Snackbar.make(constraintlayout_login_content, "Weight cannot be 0", Snackbar.LENGTH_SHORT).show()
                }
            } else {
                Snackbar.make(constraintlayout_login_content, "Weight cannot be empty", Snackbar.LENGTH_SHORT).show()
            }
        }

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .build()

        googleSignInClient = GoogleSignIn.getClient(this, gso)

        auth = FirebaseAuth.getInstance()

        databaseHelper = DatabaseHelper(this)
    }

    override fun onStart() {
        super.onStart()

        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun updateUI(account: FirebaseUser?) {
        if(account != null) {

            val userProfile = getUserProfileFromDb(account.uid)

            button_login_signout.visibility = View.VISIBLE
            button_login_signin.visibility = View.GONE
            textview_login_login_summary.visibility = View.GONE
            editText_login_weight.setText((userProfile.weight ?: SportActivities.DEFAULT_WEIGHT).toString())

            Log.d(TAG, "User logged IN")
        } else {
            val sharedPref = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)

            button_login_signout.visibility = View.GONE
            button_login_signin.visibility = View.VISIBLE
            textview_login_login_summary.visibility = View.VISIBLE
            editText_login_weight.setText(sharedPref.getFloat(Constants.SAVED_WEIGHT_KEY, SportActivities.DEFAULT_WEIGHT).toString())

            Log.d(TAG, "User logged OUT")
        }
    }

    private fun updateSharedPreferences(account: FirebaseUser?) {
        if(account != null) {

            val userProfile = getUserProfileFromDb(account.uid)

            val sharedPref = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE) ?: return
            with (sharedPref.edit()) {
                putLong(Constants.SAVED_USERID_KEY, userProfile.user.id)
                putFloat(Constants.SAVED_WEIGHT_KEY, userProfile.weight ?: SportActivities.DEFAULT_WEIGHT)
                putBoolean(Constants.SAVED_LOGINSTATE_KEY, true)
                apply()
            }

            Log.d(TAG, account.displayName)

        } else {
            val sharedPref = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE) ?: return
            with (sharedPref.edit()) {
                remove(Constants.SAVED_USERID_KEY)
                putFloat(Constants.SAVED_WEIGHT_KEY, SportActivities.DEFAULT_WEIGHT)
                putBoolean(Constants.SAVED_LOGINSTATE_KEY, false)
                apply()
            }
        }
    }

    private fun getUserProfileFromDb(googleUID: String): UserProfile {
        val userDao = databaseHelper.userDao()
        val userProfileDao = databaseHelper.userProfileDao()

        val qUser = userDao.queryForEq("accId", googleUID)

        // User exists
        if(qUser.isNotEmpty()) {
            val user = qUser.first()
            val qProfile = userProfileDao.queryForEq("user_id", user)
            return qProfile.first()
        }

        // User does not exists, we create a new one
        val newUser = User(googleUID)
        userDao.create(newUser)
        val newUserProfile = UserProfile(newUser)
        userProfileDao.create(newUserProfile)
        return newUserProfile
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            firebaseAuthWithGoogle(account!!)
        } catch(e: ApiException) {
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            updateUI(null)
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if(task.isSuccessful) {
                    // Sign in success
                    Log.d(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    updateSharedPreferences(user)
                    updateUI(user)

                } else {
                    // Sign in failed
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    Snackbar.make(constraintlayout_login_content, "Authentication failed.", Snackbar.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }
    }
}
