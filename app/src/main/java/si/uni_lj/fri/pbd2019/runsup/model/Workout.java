package si.uni_lj.fri.pbd2019.runsup.model;

import android.util.Log;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable(tableName = "Workout")
public class Workout {

    /**
     * Constants
     */

    // initial status of workout
    public static final int statusUnknown = 0;
    // ended workout
    public static final int statusEnded = 1;
    // paused workout
    public static final int statusPaused = 2;
    // deleted workout
    public static final int statusDeleted = 3;

    /**
     * Constructors
     */

    public Workout() {
        // Required by ORMLite
    }

    public Workout(String title, int sportActivity) {
        this.title = title;
        this.sportActivity = sportActivity;
        this.created = new Date();
        this.status = statusUnknown;
        this.distance = 0.0;
        this.duration = 0L;
        this.totalCalories = 0.0;
        this.paceAvg = 0.0;
        this.paceCounter = 0;
        this.imageUrl = null;
    }

    /**
     * Model variables
     */

    @DatabaseField(generatedId = true)
    private Long id;

    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private User user;

    @DatabaseField
    private String title;

    @DatabaseField
    private Date created;

    @DatabaseField
    private Integer status;

    @DatabaseField(defaultValue = "0.0")
    private Double distance;

    @DatabaseField(defaultValue = "0")
    private Long duration;

    @DatabaseField(defaultValue = "0.0")
    private Double totalCalories;

    @DatabaseField(defaultValue = "0.0")
    private Double paceAvg;

    @DatabaseField
    private Integer sportActivity;

    @DatabaseField(version = true)
    private Date lastUpdate;

    @DatabaseField(defaultValue = "0")
    private Integer paceCounter;

    @DatabaseField
    private String imageUrl;

    /**
     * Getters and setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Double getTotalCalories() {
        return totalCalories;
    }

    public void setTotalCalories(Double totalCalories) {
        this.totalCalories = totalCalories;
    }

    public Double getPaceAvg() {
        return paceAvg;
    }

    public void setPaceAvg(Double paceAvg) {
        this.paceAvg = paceAvg;
    }

    public Integer getSportActivity() {
        return sportActivity;
    }

    public void setSportActivity(Integer sportActivity) {
        this.sportActivity = sportActivity;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getPaceCounter() {
        return paceCounter;
    }

    public void setPaceCounter(Integer paceCounter) {
        this.paceCounter = paceCounter;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
