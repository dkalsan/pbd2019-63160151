package si.uni_lj.fri.pbd2019.runsup

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_challenge.*
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper
import si.uni_lj.fri.pbd2019.runsup.helpers.MapHelper
import si.uni_lj.fri.pbd2019.runsup.model.*
import si.uni_lj.fri.pbd2019.runsup.services.ChallengeService

class ChallengeActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 522
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mMatchmakersRef: DatabaseReference
    private lateinit var mCompetitionsRef: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private lateinit var trackId: String
    private lateinit var challengeAcceptedValueListener: ValueEventListener
    private var competitor: Competitor? = null
    private var searching: Boolean = false
    private var matchmakerId: String? = null
    private var competitionId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenge)

        // Get database references
        mDatabase = FirebaseDatabase.getInstance().reference
        mMatchmakersRef = mDatabase.child(Constants.FIREBASE_MATCHMAKERS_KEY)
        mCompetitionsRef = mDatabase.child(Constants.FIREBASE_COMPETITIONS_KEY)

        // Get track id
        trackId = intent.getStringExtra("trackId")

        // Initialize listener for accepted challenge for further use
        challengeAcceptedValueListener = object: ValueEventListener {
            override fun onCancelled(error: DatabaseError) = Unit
            override fun onDataChange(snapshot: DataSnapshot) {
                val newMatchmakerRef = mMatchmakersRef.child(matchmakerId!!)
                val pendingMatchmaker = snapshot.getValue(Matchmaker::class.java)

                // Somebody accepted our challenge
                if(pendingMatchmaker == null && searching) {
                    searching = false
                    newMatchmakerRef.removeEventListener(this)

                    startChallengeService(trackId, competitionId, auth.currentUser?.uid, "A")

                    Intent(this@ChallengeActivity, TrackMapActivity::class.java).apply {
                        putExtra("trackId", trackId)
                        putExtra("competitionId", competitionId)
                        putExtra("identifier", "A")
                        putExtra("competitorId", auth.currentUser?.uid)
                        startActivity(this)
                    }

                    competitionId = null
                    matchmakerId = null
                }
            }
        }

        // Restore views and variables
        savedInstanceState?.run {
            searching = getBoolean("searching")
            competitionId = getString("competitionId")
            matchmakerId = getString("matchmakerId")
            matchmakerId?.let {
                Log.d("RunsUp!", "Setting listener")
                mMatchmakersRef.child(it).addValueEventListener(challengeAcceptedValueListener)
            }
            this
        }

        // Get auth
        auth = FirebaseAuth.getInstance()

        // Initialize competition account
        initCompetitionAccount()

        // Initialize map fragment
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragment_challenge_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // On click listener for competition search
        button_challenge_opponent.setOnClickListener {

            // Check for location permission
            if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            } else {
                startSearchingForAMatch()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        toggleSearchingButtons()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    startSearchingForAMatch()
                } else {
                    Snackbar.make(scrollview_challenge_content, "Sorry but the location permission is mandatory if you want to compete.", Snackbar.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapLoadedCallback(this)
    }

    override fun onMapLoaded() {

        trackId = intent.getStringExtra("trackId")

        MapHelper.setUpMap(mMap, trackId, this)

        mMap.setOnMapClickListener {
            Intent(this, MapsActivity::class.java).apply {
                putExtra("trackId", trackId)
                startActivity(this)
            }
        }
    }

    override fun onBackPressed() {
        if(searching) {
            val builder = AlertDialog.Builder(this, R.style.AlertDialogCustom)
            builder.setTitle("Search in progress")
            builder.setMessage("Are you sure you would like to leave this screen? This will terminate your current search.")
            builder.setPositiveButton(R.string.yes) { _, _ ->
                searching = false

                mMatchmakersRef.addListenerForSingleValueEvent(object: ValueEventListener {
                    override fun onCancelled(error: DatabaseError) = Unit
                    override fun onDataChange(snapshot: DataSnapshot) {
                        snapshot.children.forEach { snapshotChild ->
                            val pendingMatchmaker = snapshotChild.getValue(Matchmaker::class.java)

                            if(pendingMatchmaker?.initiatorUid == auth.currentUser?.uid) {

                                // Delete pending competition
                                mCompetitionsRef
                                    .child(pendingMatchmaker?.competitionId!!)
                                    .setValue(null)

                                // Remove query from the matchmaker
                                mMatchmakersRef
                                    .child(snapshotChild.key!!)
                                    .setValue(null)
                            }
                        }
                    }

                })

                super.onBackPressed()
            }
            builder.setNegativeButton(R.string.no, null)
            builder.create().show()
        } else {
            super.onBackPressed()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putBoolean("searching", searching)
            putString("competitionId", competitionId)
            putString("matchmakerId", matchmakerId)
        }

        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {

        matchmakerId?.let {
            Log.d("RunsUp!", "Removing listener")
            mMatchmakersRef.child(it).removeEventListener(challengeAcceptedValueListener)
        }

        super.onDestroy()
    }

    private fun updateUI() {
        mDatabase
            .child(Constants.FIREBASE_TRACKS_KEY)
            .child(trackId)
            .addValueEventListener(object: ValueEventListener {
                override fun onCancelled(error: DatabaseError) = Unit
                override fun onDataChange(snapshot: DataSnapshot) {
                    val track = snapshot.getValue(Track::class.java)

                    if(track != null) {
                        toggleError(false)

                        textview_challenge_titlevalue.text = track.title
                        textview_challenge_locationvalue.text = track.location
                        textview_challenge_difficultyvalue.text = track.difficulty

                        val bestTime = track.bestTime
                        textview_challenge_besttimevalue.text = MainHelper.formatDuration(bestTime)

                        textview_challenge_personalbesttimevalue.text = MainHelper.formatDuration(0L)
                        competitor?.trackResults?.forEach {
                            if(it.trackId == trackId) {
                                textview_challenge_personalbesttimevalue.text = MainHelper.formatDuration(it.personalBest ?: 0L)
                            }
                        }

                        val rating = track.rating
                        ratingbar_challenge_rating.rating = rating
                    } else {
                        toggleError(true)
                        textview_challenge_error.text = getString(R.string.challenge_error)
                    }
                }
            })
    }

    private fun initCompetitionAccount() {

        val user = auth.currentUser

        if(user != null) {
            mDatabase
                .child(Constants.FIREBASE_COMPETITORS_KEY)
                .child(user.uid)
                .addListenerForSingleValueEvent(object: ValueEventListener {
                    override fun onCancelled(error: DatabaseError) = Unit
                    override fun onDataChange(snapshot: DataSnapshot) {
                        competitor = snapshot.getValue(Competitor::class.java)

                        // Create an entry in the database if it doesn't exist yet
                        if(competitor == null) {

                            // Set and save competitor values
                            competitor = Competitor(user.displayName, user.photoUrl.toString())
                            mDatabase
                                .child("${Constants.FIREBASE_COMPETITORS_KEY}/${user.uid}")
                                .setValue(competitor)
                        }

                        updateUI()
                    }
                })
        }
    }


    private fun toggleError(somethingWrong: Boolean) {

        val visibility = if(somethingWrong) View.GONE else View.VISIBLE
        textview_challenge_personalbesttimevalue.visibility = visibility
        textview_challenge_besttimevalue.visibility = visibility
        textview_challenge_difficultyvalue.visibility = visibility
        textview_challenge_locationvalue.visibility = visibility
        textview_challenge_titlelabel.visibility = visibility
        textview_challenge_besttimelabel.visibility = visibility
        textview_challenge_challenge.visibility = visibility
        textview_challenge_difficultylabel.visibility = visibility
        textview_challenge_locationlabel.visibility = visibility
        textview_challenge_personalbesttimelabel.visibility = visibility
        textview_challenge_titlevalue.visibility = visibility
        textview_challenge_trackinfo.visibility = visibility
        button_challenge_opponent.visibility = visibility
        // fragment_challenge_map.view?.visibility = visibility
        view_challenge_divider.visibility = visibility
        ratingbar_challenge_rating.visibility = visibility

        textview_challenge_error.visibility = if(somethingWrong) View.VISIBLE else View.GONE
    }

    private fun createChallenge() {
        val newCompetitionRef = mCompetitionsRef.push()
        val newCompetition = Competition(trackId, competitor)

        newCompetitionRef
            .setValue(newCompetition)

        val newMatchmakerRef = mMatchmakersRef.push()
        val newMatchmaker = Matchmaker(newCompetitionRef.key, auth.currentUser?.uid, trackId)

        mMatchmakersRef.runTransaction(object: Transaction.Handler {
            override fun doTransaction(data: MutableData): Transaction.Result {

                data.children.forEach { dataChild ->
                    val pendingMatchmaker = dataChild.getValue(Matchmaker::class.java)

                    // Somebody created the track query we're searching for in the meantime
                    if(pendingMatchmaker?.trackId == trackId && dataChild.key != newMatchmakerRef.key) {
                        return Transaction.abort()
                    }
                }

                if(newMatchmakerRef.key != null) {
                    data
                        .child(newMatchmakerRef.key!!)
                        .value = newMatchmaker

                    competitionId = newCompetitionRef.key
                    matchmakerId = newMatchmakerRef.key

                    return Transaction.success(data)
                }

                return Transaction.abort()
            }
            override fun onComplete(error: DatabaseError?, commit: Boolean, snapshot: DataSnapshot?) {
                if(commit) {
                    Snackbar.make(scrollview_challenge_content, "You are the first one! Waiting for an opponent...", Snackbar.LENGTH_LONG).show()
                    Log.d("RunsUp!", "Setting listener")
                    newMatchmakerRef.addValueEventListener(challengeAcceptedValueListener)
                } else {
                    Snackbar.make(scrollview_challenge_content, "An error occured while posting the game. Retrying.", Snackbar.LENGTH_LONG).show()

                    // Clear values
                    newCompetitionRef.removeValue()
                    newMatchmakerRef.removeValue()
                    competitionId = null
                    matchmakerId = null

                    // Restart search
                    startSearchingForAMatch()
                }
            }
        })

    }

    private fun acceptChallenge(pendingMatchmaker: Matchmaker?, key: String?) {

        if(key != null){
            mMatchmakersRef
                .child(key)
                .runTransaction(object: Transaction.Handler {
                    override fun doTransaction(data: MutableData): Transaction.Result {

                        val matchmaker = data.getValue(Matchmaker::class.java)

                        // Join the competition
                        if(matchmaker?.competitionId == pendingMatchmaker?.competitionId &&
                            matchmaker?.initiatorUid != auth.currentUser?.uid &&
                            matchmaker?.trackId == trackId) {
                            data.value = null
                            return Transaction.success(data)
                        }

                        // Someone already joined the competition in the meantime
                        return Transaction.abort()
                    }

                    override fun onComplete(error: DatabaseError?, commit: Boolean, snapshot: DataSnapshot?) {
                        if(commit && pendingMatchmaker != null) {
                            val competitionRef = mCompetitionsRef.child(pendingMatchmaker.competitionId!!)
                            competitionRef
                                .child("competitorB")
                                .setValue(competitor)

                            Snackbar.make(scrollview_challenge_content, "Opponent found! Starting shortly...", Snackbar.LENGTH_LONG).show()
                            searching = false

                            // Start the service and load data
                            startChallengeService(trackId, competitionRef.key, auth.currentUser?.uid, "B")

                            Intent(this@ChallengeActivity, TrackMapActivity::class.java).apply {
                                putExtra("trackId", trackId)
                                putExtra("competitionId", competitionRef.key)
                                putExtra("competitorId", auth.currentUser?.uid)
                                putExtra("identifier", "B")
                                startActivity(this)
                            }
                        } else {
                            // Database is eventually consistent so we try again
                            startSearchingForAMatch()
                        }
                    }
                })
        } else {
            Snackbar.make(scrollview_challenge_content, "An error occured. Please try again in a moment.", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun startSearchingForAMatch() {

        // Change the searching flag
        searching = true

        // Update views
        toggleSearchingButtons()

        // Start searching
        mMatchmakersRef.addListenerForSingleValueEvent(object: ValueEventListener {
            override fun onCancelled(error: DatabaseError) = Unit
            override fun onDataChange(snapshot: DataSnapshot) {

                // Check if there's a pending match for our track
                snapshot.children.forEach { snapshotChild ->
                    val pendingMatchmaker = snapshotChild.getValue(Matchmaker::class.java)

                    if(pendingMatchmaker?.initiatorUid != auth.currentUser?.uid
                        && pendingMatchmaker?.trackId == trackId) {

                        acceptChallenge(pendingMatchmaker, snapshotChild.key)
                        return
                    }
                }

                // There was no pending match. We create a new one
                createChallenge()
            }

        })
    }

    private fun toggleSearchingButtons() {
        if(searching) {
            button_challenge_opponent.isEnabled = false
            button_challenge_opponent.backgroundTintList = getColorStateList(R.color.colorPrimaryDark)
            button_challenge_opponent.text = getString(R.string.challenge_searching)
        } else {
            button_challenge_opponent.isEnabled = true
            button_challenge_opponent.backgroundTintList = getColorStateList(R.color.colorPrimary)
            button_challenge_opponent.text = getString(R.string.challenge_findopponent)
        }
    }

    private fun startChallengeService(trackId: String, competitionId: String?, competitorId: String?, identifier: String) {

        val serviceIntent = Intent(this, ChallengeService::class.java).apply {
            putExtra("trackId", trackId)
            putExtra("competitionId", competitionId)
            putExtra("identifier", identifier)
            action = Constants.LOAD_TRACK_BY_ID
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(serviceIntent)
        } else {
            startService(serviceIntent)
        }
    }
}