package si.uni_lj.fri.pbd2019.runsup

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationManager
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.firebase.database.*
import com.google.maps.android.ui.IconGenerator
import kotlinx.android.synthetic.main.activity_track_map.*
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper
import si.uni_lj.fri.pbd2019.runsup.model.Competition
import si.uni_lj.fri.pbd2019.runsup.model.Competitor
import si.uni_lj.fri.pbd2019.runsup.model.Track
import si.uni_lj.fri.pbd2019.runsup.model.TrackResult
import si.uni_lj.fri.pbd2019.runsup.services.ChallengeService
import kotlin.math.min

class TrackMapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 10
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mBroadcastReceiver: BroadcastReceiver
    private lateinit var mStateReceiver: BroadcastReceiver
    private lateinit var track: Track
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mCompetitionRef: DatabaseReference
    private lateinit var identifier: String
    private lateinit var opponentIdentifier: String
    private lateinit var startLocation: Location
    private lateinit var countdownHandler: Handler
    private lateinit var opponentMarker: Marker
    private var started = false
    private var finished = false
    private var resultSaved = false

    // TODO: Clear database best times and ratings
    // TODO: Write app description, generate apk, and add GPX to bitbucket

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_map)

        if(savedInstanceState != null) {
            started = savedInstanceState.getBoolean("started")
            if(started) {
                button_trackmap_start.visibility = View.GONE
            }
        }

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragment_trackmap_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // Get database references
        mDatabase = FirebaseDatabase.getInstance().reference
        mCompetitionRef = mDatabase
            .child(Constants.FIREBASE_COMPETITIONS_KEY)
            .child(intent.getStringExtra("competitionId"))

        // Get reference to track info updates
        mDatabase
            .child(Constants.FIREBASE_TRACKS_KEY)
            .child(intent.getStringExtra("trackId"))
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) = Unit
                override fun onDataChange(snapshot: DataSnapshot) {
                    track = snapshot.getValue(Track::class.java)!!
                }

            })

        // Initialize broadcast receiver
        mBroadcastReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if(intent?.action == Constants.BROADCAST_CHALLENGE_DETAILS) {

                    val currLocation = intent.getParcelableExtra<Location>("location")
                    finished = intent.getBooleanExtra("finished", false)

                    if(started) {

                        mMap.animateCamera(
                            CameraUpdateFactory.newCameraPosition(
                                CameraPosition.fromLatLngZoom(
                                    LatLng(currLocation.latitude, currLocation.longitude), 18f
                                )
                            )
                        )

                        // Check whether this is the last checkpoint
                        if(finished) {
                            finishChallenge()
                        }
                    } else if(::startLocation.isInitialized && currLocation.distanceTo(startLocation) < 20.0f) {
                        // Competitor is at the start location
                        button_trackmap_start.visibility = View.VISIBLE
                        button_trackmap_gettostart.visibility = View.GONE

                    } else {
                        // Disable start
                        button_trackmap_start.visibility = View.GONE
                        button_trackmap_gettostart.visibility = View.VISIBLE
                    }
                }
            }
        }

        // Initialize state receiver
        mStateReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if(intent?.action == Constants.BROADCAST_CHALLENGE_STATUS) {

                    if(intent.getBooleanExtra("finished", false)) {
                        finishChallenge()
                    } else {
                        startReceivingBroadcast()
                    }
                }
            }
        }

        // Set state listener
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mStateReceiver,
            IntentFilter(Constants.BROADCAST_CHALLENGE_STATUS)
        )

        // Get char identifier which will be used for saving to DB
        identifier = intent.getStringExtra("identifier")
        opponentIdentifier = if(identifier == "A") "B" else "A"

        // Create onclick listener
        button_trackmap_start.setOnClickListener {
            button_trackmap_start.visibility = View.GONE
            started = true

            // Start calculating offtrack distance in service
            startChallengeService(Constants.COMMAND_START)

            textview_trackmap_countdown.text = "3"
            countdownHandler = Handler()
            countdownHandler.postDelayed({ textview_trackmap_countdown.text = "2" }, 1000)
            countdownHandler.postDelayed({ textview_trackmap_countdown.text = "1" }, 2000)
            countdownHandler.postDelayed({
                textview_trackmap_countdown.text = getString(R.string.trackmap_go)
                mCompetitionRef
                    .child("startTime$identifier")
                    .setValue(ServerValue.TIMESTAMP)
            }, 3000)
            countdownHandler.postDelayed({ textview_trackmap_countdown.visibility = View.GONE }, 4000)
        }

        // Start listening to opponent location and showing it on map
        mCompetitionRef
            .child("competitor$opponentIdentifier")
            .addValueEventListener(object: ValueEventListener {
                override fun onCancelled(error: DatabaseError) = Unit
                override fun onDataChange(snapshot: DataSnapshot) {
                    val opponent = snapshot.getValue(Competitor::class.java)

                    if(opponent != null) {
                        if(::opponentMarker.isInitialized) {
                            opponentMarker.remove()
                        }

                        opponentMarker = mMap.addMarker(
                            MarkerOptions()
                                .position(LatLng(opponent.lat, opponent.lng))
                                .icon(BitmapDescriptorFactory.defaultMarker())
                        )

                    }

                }

            })

        // Set rating on change listener
        ratingbar_trackmap_rating.setOnRatingBarChangeListener { _, rating, fromUser ->
            val competitorId = intent.getStringExtra("competitorId")

            if(fromUser && competitorId != null) {

                mDatabase
                    .child(Constants.FIREBASE_COMPETITORS_KEY)
                    .child(competitorId)
                    .addListenerForSingleValueEvent(object: ValueEventListener {
                        override fun onCancelled(error: DatabaseError) = Unit

                        override fun onDataChange(snapshot: DataSnapshot) {
                            val competitor = snapshot.getValue(Competitor::class.java)
                            val currTrackId = intent.getStringExtra("trackId")

                            if(competitor != null) {
                                competitor.trackResults?.forEachIndexed { ix, tr ->
                                    if(tr.trackId == currTrackId) {

                                        // Competitor already rated it before so remove it
                                        if(tr.rating != null) {
                                            if(track.ratingCounter > 1){
                                                track.rating = ((track.rating * track.ratingCounter) - tr.rating!!) / (track.ratingCounter - 1)
                                            } else {
                                                track.rating = 0f
                                            }
                                            track.ratingCounter -= 1
                                        }

                                        // Calculate new values for track
                                        track.ratingCounter += 1
                                        track.rating = track.rating + ((rating - track.rating) / track.ratingCounter)

                                        // Update track in firebase
                                        mDatabase
                                            .child(Constants.FIREBASE_TRACKS_KEY)
                                            .child(currTrackId)
                                            .setValue(track)

                                        // Set for competitor
                                        mDatabase
                                            .child("${Constants.FIREBASE_COMPETITORS_KEY}/$competitorId/trackResults/$ix/rating")
                                            .setValue(rating)

                                    }
                                }
                            }
                        }

                    })
            }
        }

        // Set return button onclick
        button_trackmap_return.setOnClickListener {
            Intent(this, ChallengeActivity::class.java).apply {
                putExtra("trackId", intent.getStringExtra("trackId"))
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(this)
                finish()
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Check for location permission
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        mMap.setOnMapLoadedCallback(this)
        mMap.isMyLocationEnabled = true
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.uiSettings.isRotateGesturesEnabled = true
        mMap.uiSettings.isCompassEnabled = true

    }

    override fun onMapLoaded() {
        setupMap()
    }


    override fun onDestroy() {

        stopService(Intent(this@TrackMapActivity, ChallengeService::class.java))
        stopReceivingBroadcast()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
            mStateReceiver
        )


        super.onDestroy()
    }

    override fun onBackPressed() {
        val builder = AlertDialog.Builder(this, R.style.AlertDialogCustom)
        builder.setTitle("Competition in progress")
        builder.setMessage("By leaving this screen you FORFEIT this match. Are you sure you want to proceed?")
        builder.setPositiveButton(R.string.yes) { _, _ ->
            mCompetitionRef
                .child("forfeited")
                .setValue(true)

            stopService(Intent(this@TrackMapActivity, ChallengeService::class.java))
            stopReceivingBroadcast()

            super.onBackPressed()
        }
        builder.setNegativeButton(R.string.no, null)
        builder.create().show()
    }

    override fun onResume() {
        super.onResume()

        startChallengeService(Constants.COMMAND_STATUS)

    }

    override fun onPause() {
        super.onPause()
        stopReceivingBroadcast()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putBoolean("started", started)
        }
        super.onSaveInstanceState(outState)
    }

    private fun setupMap() {
        // Init icons
        val icg = IconGenerator(this)
        icg.setTextAppearance(R.style.TextAppearance_AppCompat_Button)

        icg.setColor(Color.GREEN)
        val startIcon = icg.makeIcon("Start")

        icg.setColor(Color.RED)
        val endIcon = icg.makeIcon("End")

        track.geopoints.sortBy { it.seqId }
        val geopoints = track.geopoints.map { LatLng(it.lat, it.lng) }

        // Set start icon
        mMap.addMarker(
            MarkerOptions()
                .position(geopoints.first()))
            .setIcon(BitmapDescriptorFactory.fromBitmap(startIcon))

        // Set end icon

        mMap.addMarker(
            MarkerOptions()
                .position(geopoints.last()))
            .setIcon(BitmapDescriptorFactory.fromBitmap(endIcon))

        // Draw polyline
        mMap.addPolyline(
            PolylineOptions()
                .color(ContextCompat.getColor(this@TrackMapActivity, R.color.colorPrimary))
                .addAll(
                    ArrayList(geopoints)
                )
        )

        startLocation = Location(LocationManager.GPS_PROVIDER).apply {
            latitude = track.geopoints.first().lat
            longitude = track.geopoints.first().lng
        }

        // startReceivingBroadcast()
    }

    private fun setResultListener() {
        mCompetitionRef
            .addValueEventListener(object: ValueEventListener {
                override fun onCancelled(error: DatabaseError) = Unit
                override fun onDataChange(snapshot: DataSnapshot) {
                    val competition = snapshot.getValue(Competition::class.java)

                    if(competition != null) {
                        var totalTimeA = -1L
                        var totalTimeB = -1L

                        if(competition.endTimeA != null && competition.startTimeA != null) {
                            totalTimeA = (competition.endTimeA!! - competition.startTimeA!!) / 1000
                        }
                        if(competition.endTimeB != null && competition.startTimeB != null) {
                            totalTimeB = (competition.endTimeB!! - competition.startTimeB!!) / 1000
                        }

                        textview_trackmap_yourtimevalue.text = MainHelper.formatDuration(if(identifier == "A") totalTimeA else totalTimeB)

                        // Save result to Firebase
                        if(!resultSaved) {
                            resultSaved = true
                            saveResultToFirebase(if(identifier == "A") totalTimeA else totalTimeB)
                        }

                        if(totalTimeA != -1L && totalTimeB != -1L || competition.forfeited) {

                            if(competition.forfeited) {
                                totalTimeA = if(totalTimeA == -1L) Float.POSITIVE_INFINITY.toLong() else totalTimeA
                                totalTimeB = if(totalTimeB == -1L) Float.POSITIVE_INFINITY.toLong() else totalTimeB
                                textview_trackmap_opponenttimevalue.text = getString(R.string.trackmap_resultforfeited)
                            } else {
                                textview_trackmap_opponenttimevalue.text = MainHelper.formatDuration(if(identifier == "A") totalTimeB else totalTimeA)
                            }

                            when {
                                totalTimeA < totalTimeB -> {
                                    textview_trackmap_result.text = if(identifier == "A") getString(R.string.trackmap_resultwin) else getString(R.string.trackmap_resultlost)
                                }
                                totalTimeA > totalTimeB -> {
                                    textview_trackmap_result.text = if(identifier == "A") getString(R.string.trackmap_resultlost) else getString(R.string.trackmap_resultwin)
                                }
                                else -> textview_trackmap_result.text = getString(R.string.trackmap_resulttie)
                            }

                            // Update best time
                            if(track.bestTime > min(totalTimeA, totalTimeB) || track.bestTime == 0L) {
                                track.bestTime = min(totalTimeA, totalTimeB)
                                mDatabase
                                    .child(Constants.FIREBASE_TRACKS_KEY)
                                    .child(competition.trackId!!)
                                    .setValue(track)
                            }

                        }

                    }
                }
            })
    }

    private fun saveResultToFirebase(totalTime: Long) {
        val competitorId = intent.getStringExtra("competitorId")
        if(competitorId != null) {
            mDatabase
                .child("${Constants.FIREBASE_COMPETITORS_KEY}/$competitorId")
                .addListenerForSingleValueEvent(object: ValueEventListener {
                    override fun onCancelled(error: DatabaseError) = Unit
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val competitor = snapshot.getValue(Competitor::class.java)
                        val currTrackId = intent.getStringExtra("trackId")

                        if(competitor != null) {

                            var trackEntryFound = false
                            competitor.trackResults?.forEachIndexed { ix, tr ->
                                if(tr.trackId == currTrackId) {

                                    // Initialize view rating value
                                    tr.rating?.let {
                                        ratingbar_trackmap_rating.rating = tr.rating!!
                                    }

                                    trackEntryFound = true
                                    if(tr.personalBest == null || tr.personalBest!! >= totalTime) {
                                        mDatabase
                                            .child("${Constants.FIREBASE_COMPETITORS_KEY}/$competitorId/trackResults/$ix/personalBest")
                                            .setValue(totalTime)
                                    }
                                }
                            }
                            if(!trackEntryFound) {
                                val tr = TrackResult(currTrackId, totalTime)
                                if(competitor.trackResults != null) {
                                    mDatabase
                                        .child("${Constants.FIREBASE_COMPETITORS_KEY}/$competitorId/trackResults/${competitor.trackResults!!.size}")
                                        .setValue(tr)
                                } else {
                                    mDatabase
                                        .child("${Constants.FIREBASE_COMPETITORS_KEY}/$competitorId/trackResults/0")
                                        .setValue(tr)
                                }
                            }
                        }
                    }

                })
        }
    }

    private fun startReceivingBroadcast() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mBroadcastReceiver,
            IntentFilter(Constants.BROADCAST_CHALLENGE_DETAILS)
        )
    }

    private fun stopReceivingBroadcast() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
            mBroadcastReceiver
        )
    }

    private fun startChallengeService(command: String) {
        val serviceIntent = Intent(this, ChallengeService::class.java).apply {
            action = command
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(serviceIntent)
        } else {
            startService(serviceIntent)
        }
    }

    private fun finishChallenge() {
        // Stop service
        stopService(Intent(this@TrackMapActivity, ChallengeService::class.java))
        stopReceivingBroadcast()

        // Show result overlay
        constraintlayout_trackmap_result.visibility = View.VISIBLE

        // Set listener for result details
        setResultListener()
    }

}
