package si.uni_lj.fri.pbd2019.runsup

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_stopwatch.*
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService

class StopwatchActivity : AppCompatActivity() {

    private val STOP_TAG = "stop"
    private val START_TAG = "start"
    private val CONTINUE_TAG = "continue"

    private var localState = Constants.STATE_NEVER_STARTED

    lateinit var mBroadcastReceiver: BroadcastReceiver

    // Workout details values
    private var sportActivity: Int = 0
    private var duration: Long = 0
    private var distance: Double = 0.0
    private var calories: Double = 0.0
    private var pace: Double = 0.0
    private var serviceState: Int = -1
    private var workoutId: Long = -1L
    private var location: Location? = null
    private lateinit var paceHistory: ArrayList<Double>
    private lateinit var workoutPositionList: ArrayList<List<Location>>
    private lateinit var tmpPositionArrayList: ArrayList<Location>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stopwatch)

        if (savedInstanceState != null) {
            with(savedInstanceState) {
                getString("buttonState").also { tag ->
                    button_stopwatch_start.tag = tag
                    when(tag) {
                        STOP_TAG -> button_stopwatch_start.text = getString(R.string.stopwatch_stop)
                        START_TAG -> button_stopwatch_start.text = getString(R.string.stopwatch_start)
                        CONTINUE_TAG -> {
                            button_stopwatch_start.text = getString(R.string.stopwatch_continue)
                            button_stopwatch_endworkout.visibility = View.VISIBLE
                        }
                    }
                }

                textview_stopwatch_duration.text = getString("duration")
                textview_stopwatch_distance.text = getString("distance")
                textview_stopwatch_pace.text = getString("pace")
                textview_stopwatch_calories.text = getString("calories")

            }
        }

        // Check permissions
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                1
            )
        }

        // Initialize tmpPositionArrayList
        tmpPositionArrayList = arrayListOf()

        // OnClick listener for ending the workout
        button_stopwatch_endworkout.setOnClickListener {
            val builder = AlertDialog.Builder(this@StopwatchActivity, R.style.AlertDialogCustom)
            builder.setTitle(R.string.dialog_title)
            builder.setMessage(R.string.dialog_message)
            builder.setPositiveButton(R.string.yes) { _, _ ->

                Log.d("RunsUp!", "[SENDING ID]: $workoutId")

                // Stop the service
                Intent(this, TrackerService::class.java).also { intent ->
                    intent.action = Constants.COMMAND_STOP
                    intent.putExtra("sportActivity", sportActivity)
                    startService(intent)
                }

                // Send an intent for workout details
                Intent(this, WorkoutDetailActivity::class.java).also { intent ->
                    intent.putExtra("workoutId", workoutId)
                    startActivity(intent)
                }

            }
            builder.setNegativeButton(R.string.no, null)
            builder.create().show()
        }

        // OnClick listener for starting, stopping/pausing and continuing the workout
        button_stopwatch_start.setOnClickListener {

            // Initialize basic intent
            val intent = Intent(this, TrackerService::class.java).apply {
                putExtra("sportActivity", sportActivity)
            }

            val btn = it as Button
            when(btn.tag) {

                START_TAG -> {
                    btn.text = getString(R.string.stopwatch_stop)
                    btn.tag = STOP_TAG
                    intent.action = Constants.COMMAND_START
                    localState = Constants.STATE_RUNNING
                }
                STOP_TAG -> {
                    btn.text = getString(R.string.stopwatch_continue)
                    btn.tag = CONTINUE_TAG
                    button_stopwatch_endworkout.visibility = View.VISIBLE
                    intent.action = Constants.COMMAND_PAUSE
                    localState = Constants.STATE_PAUSED

                    // Save the current temporary list of locations to the main list
                    workoutPositionList.add(tmpPositionArrayList.toList())
                }
                CONTINUE_TAG -> {
                    btn.text = getString(R.string.stopwatch_stop)
                    btn.tag = STOP_TAG
                    button_stopwatch_endworkout.visibility = View.GONE
                    intent.action = Constants.COMMAND_CONTINUE
                    localState = Constants.STATE_CONTINUE
                }
            }

            // Start the TrackerService
            startService(intent)
        }

        mBroadcastReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {

                if(intent?.action == Constants.BROADCAST_TRACKER_DETAILS) {

                    // Extract detail values
                    duration = intent.getLongExtra("duration", -1)
                    distance = intent.getDoubleExtra("distance", -1.0)
                    pace = intent.getDoubleExtra("pace", -1.0)
                    paceHistory.add(pace)
                    calories = intent.getDoubleExtra("calories", -1.0)
                    serviceState = intent.getIntExtra("state", -1)
                    sportActivity = intent.getIntExtra("sportActivity", -1)
                    workoutId = intent.getLongExtra("workoutId", -1)
                    location = intent.getParcelableExtra<Location>("location")

                    // Assign detail values to views
                    assignDetailsToViews()
                }
            }
        }

        // Start listening to the TrackerService
        startReceivingBroadcast()

        // Initializing position list of all GPS positions
        workoutPositionList = arrayListOf()

        // Initializing pace history
        paceHistory = arrayListOf()
    }

    override fun onResume() {
        super.onResume()
        startReceivingBroadcast()

        // Start the service without a command
        Intent(this, TrackerService::class.java).apply {
            putExtra("sportActivity", sportActivity)
            startService(this)
        }
    }

    public override fun onPause() {
        super.onPause()

        stopReceivingBroadcast()

        if(!isChangingConfigurations && localState != Constants.STATE_CONTINUE && localState != Constants.STATE_RUNNING) {
            stopService(Intent(this, TrackerService::class.java))
        }

    }

    override fun onDestroy() {
        if(!isChangingConfigurations) {
            stopService(Intent(this, TrackerService::class.java))
        }

        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString("buttonState", button_stopwatch_start.tag as String)
            putString("duration", textview_stopwatch_duration.text as String)
            putString("distance", textview_stopwatch_distance.text as String)
            putString("pace", textview_stopwatch_pace.text as String)
            putString("calories", textview_stopwatch_calories.text as String)
        }

        super.onSaveInstanceState(outState)
    }

    private fun assignDetailsToViews() {

        // Assign values to the corresponding views
        textview_stopwatch_duration.text = MainHelper.formatDuration(duration)
        textview_stopwatch_distance.text = MainHelper.formatDistance(distance)
        textview_stopwatch_pace.text = MainHelper.formatPace(pace)
        textview_stopwatch_calories.text = MainHelper.formatCalories(calories)

    }

    private fun startReceivingBroadcast() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mBroadcastReceiver,
            IntentFilter(Constants.BROADCAST_TRACKER_DETAILS)
        )
    }

    private fun stopReceivingBroadcast() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
            mBroadcastReceiver
        )
    }

    // TODO: On preference change preračunaš in skupi sestaviš enote mi/min, km/min
}