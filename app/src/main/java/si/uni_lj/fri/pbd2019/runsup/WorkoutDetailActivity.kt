package si.uni_lj.fri.pbd2019.runsup

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.util.Log
import android.util.Log.d
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.ui.IconGenerator
import kotlinx.android.synthetic.main.activity_workout_detail.*
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper
import si.uni_lj.fri.pbd2019.runsup.helpers.MapHelper
import si.uni_lj.fri.pbd2019.runsup.model.Workout
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity
import java.text.DateFormat
import java.util.*

class WorkoutDetailActivity : AppCompatActivity(), GoogleMap.OnMapLoadedCallback, OnMapReadyCallback {

    private lateinit var finalPositionList: ArrayList<List<Location>>
    private lateinit var databaseHelper: DatabaseHelper
    private lateinit var workout: Workout
    private lateinit var mMap: GoogleMap

    private var workoutId = -1L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_workout_detail)
        setupActionBar()

        workoutId = intent.getLongExtra("workoutId", -1)

        if(intent.getLongExtra("workoutId", -1) != -1L) {
            getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .edit()
                .putLong("workoutId", workoutId)
                .apply()
        } else {
            workoutId = getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getLong("workoutId", -1)
        }

        // Initialize Database helper
        databaseHelper = DatabaseHelper(applicationContext)

        // Initialize map fragment
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragment_workoutdetail_map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // Prepare the handler for sharing social share button clicks
        val handleShareButtonClick = View.OnClickListener {
            val builder = AlertDialog.Builder(this@WorkoutDetailActivity, R.style.AlertDialogCustom)

            val et = EditText(this)
            et.inputType = InputType.TYPE_CLASS_TEXT
            val displayedMessage = getString(
                R.string.share_message,
                textview_workoutdetail_sportactivity.text,
                textview_workoutdetail_valuedistance.text,
                textview_workoutdetail_valueduration.text
            )
            et.setText(displayedMessage)

            builder.setTitle("Social share")
            builder.setView(et)

            when(it.id) {
                R.id.button_workoutdetail_fbsharebtn -> {
                    builder.setPositiveButton("Share") { _, _ ->
                        Intent(android.content.Intent.ACTION_SEND).also { intent ->
                            intent.type = ("text/plain")
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, et.text)
                            startActivity(intent)
                        }
                    }
                }
                R.id.button_workoutdetail_twittershare -> {
                    builder.setPositiveButton("Share") { _, _ ->
                        Intent(android.content.Intent.ACTION_SEND).also { intent ->
                            intent.type = ("text/plain")
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, et.text)
                            startActivity(intent)
                        }
                    }
                }
                R.id.button_workoutdetail_emailshare -> {
                    builder.setPositiveButton("Share") { _, _ ->
                        Intent(android.content.Intent.ACTION_SEND).also { intent ->
                            intent.type = ("message/rfc822")
                            intent.putExtra(android.content.Intent.EXTRA_TEXT, et.text)
                            startActivity(intent)
                        }
                    }
                }
                else -> {
                    builder.setPositiveButton("Share", null)
                }
            }
            builder.setNegativeButton(R.string.cancel, null)

            builder.create().show()
        }

        // Set OnClick handler for showing the full map
        button_workoutdetail_showmap.setOnClickListener {
            startMapsActivity()
        }

        // Set OnClick handler for editing the workout title
        textview_workoutdetail_workouttitle.setOnClickListener {
            val builder = AlertDialog.Builder(this@WorkoutDetailActivity, R.style.AlertDialogCustom)
            val et = EditText(this)
            et.inputType = InputType.TYPE_CLASS_TEXT
            et.setText(textview_workoutdetail_workouttitle.text)
            builder.setView(et)
            builder.setTitle("Workout title")
            builder.setPositiveButton(R.string.dialog_positive_save) { _, _ ->

                // Update the workout title on screen
                textview_workoutdetail_workouttitle.text = et.text.toString()

                // Update the workout title in the database
                val workoutDao = databaseHelper.workoutDao()
                workout.title = et.text.toString()
                workoutDao.update(workout)
            }
            builder.setNegativeButton(R.string.cancel, null)
            builder.create().show()
        }

        // Set OnClick handler for social share
        button_workoutdetail_emailshare.setOnClickListener(handleShareButtonClick)
        button_workoutdetail_fbsharebtn.setOnClickListener(handleShareButtonClick)
        button_workoutdetail_gplusshare.setOnClickListener(handleShareButtonClick)
        button_workoutdetail_twittershare.setOnClickListener(handleShareButtonClick)
    }

    override fun onResume() {
        super.onResume()
        assignDetailsToViews()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        menu.findItem(R.id.action_sync).isVisible = false
        menu.findItem(R.id.action_clearhistory).isVisible = false
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        when (item.itemId) {
            R.id.action_settings -> {
                Intent(this, SettingsActivity::class.java).also {
                    startActivity(it)
                }
                return true
            }
            R.id.action_delete -> {

                // Delete workout
                val workoutDao = databaseHelper.workoutDao()
                workoutDao.delete(workout)

                // Destroy activity and return to main screen
                Intent(this, MainActivity::class.java).also { intent ->
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    startActivity(intent)
                    finish()
                }

                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapLoadedCallback(this)
    }

    override fun onMapLoaded() {

        MapHelper.setUpMap(mMap, workoutId, this, true)

        mMap.setOnMapClickListener {
            startMapsActivity()
        }
    }

    public override fun onPause() {
        super.onPause()
    }

    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun assignDetailsToViews() {

        val workoutDao = databaseHelper.workoutDao()
        val q = workoutDao.queryForEq("id", workoutId)

        if(q.isNotEmpty()) {
            workout = q.first()

            // Get rid of potential null values
            workout.paceAvg = workout.paceAvg ?: 0.0
            workout.duration = workout.duration ?: 0L
            workout.sportActivity = workout.sportActivity ?: 0
            workout.totalCalories = workout.totalCalories ?: 0.0
            workout.distance = workout.distance ?: 0.0

            val unit = when (getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE).getString(Constants.SAVED_UNIT_KEY, "0")) {
                "0" -> getString(R.string.all_labeldistanceunitkilometers)
                "1" -> getString(R.string.all_labeldistanceunitmiles)
                else -> "?"
            }

            textview_workoutdetail_workouttitle.text = workout.title ?: "NO_NAME"
            textview_workoutdetail_sportactivity.text = resources.getStringArray(R.array.dialog_sport_list_values)[workout.sportActivity] ?: "NO_ACTIVITY"
            textview_workoutdetail_activitydate.text = DateFormat.getDateTimeInstance().format(workout.created) ?: "NO_DATE"
            textview_workoutdetail_valueduration.text = MainHelper.formatDuration(workout.duration)
            textview_workoutdetail_valuecalories.text = "${MainHelper.formatCalories(workout.totalCalories)} ${getString(R.string.all_labelcaloriesunit)}"
            textview_workoutdetail_valuedistance.text = "${MainHelper.formatDistance(if(unit == "mi") MainHelper.kmToMi(workout.distance) else workout.distance)} $unit"
            textview_workoutdetail_valueavgpace.text = "${MainHelper.formatPace(if(unit == "mi") MainHelper.minpkmToMinpmi(workout.paceAvg) else workout.paceAvg)} ${getString(R.string.unit_pace, getString(R.string.all_min), unit)}"
        } else {
            Log.d("RunsUp!", "Can't find id $workoutId")
        }

    }

    private fun startMapsActivity() {
        Intent(this, MapsActivity::class.java).apply {
            putExtra("workoutId", workout.id)
            startActivity(this)
        }
    }

}
