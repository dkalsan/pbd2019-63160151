package si.uni_lj.fri.pbd2019.runsup.helpers

import kotlin.math.roundToInt

class MainHelper {
    companion object {

        /* constants */
        private const val MpS_TO_MIpH = 2.23694f
        private const val KM_TO_MI = 0.62137119223734f
        private const val MINpKM_TO_MINpMI = 1.609344f

        /**
         * return string of time in format HH:MM:SS
         * @param time - in seconds
         */
        @JvmStatic
        fun formatDuration(time: Long): String {
            val hrs = time/3600
            val mins = (time - hrs*3600) / 60
            val secs = (time - hrs*3600 - mins*60)
            return "%02d:%02d:%02d".format(hrs, mins, secs)
        }

        @JvmStatic
        fun formatDurationAux(time: Long): String {
            val hrs = time/3600
            val mins = (time - hrs*3600) / 60
            val secs = (time - hrs*3600 - mins*60)
            return "%01d:%02d:%02d".format(hrs, mins, secs)
        }

        /**
         * convert m to km and round to 2 decimal places and return as string
         */
        @JvmStatic
        fun formatDistance(n: Double): String {
            return "%.2f".format(n / 1000)
        }

        /**
         * round number to 2 decimal places and return as string
         */
        @JvmStatic
        fun formatPace(n: Double): String {
            return "%.2f".format(n)
        }

        /**
         * round number to integer
         */
        @JvmStatic
        fun formatCalories(n: Double): String {
            return "%d".format(n.roundToInt())
        }

        /**
         * convert km to mi (multiply with a corresponding constant)
         */
        @JvmStatic
        fun kmToMi(n: Double): Double {
            return n * KM_TO_MI
        }

        /**
         * convert mi to km (multiply with a corresponding constant)
         */
        @JvmStatic
        fun miToKm(n: Double): Double {
            return n / KM_TO_MI
        }

        /**
         * convert m/s to mi/h (multiply with a corresponding constant)
         */
        @JvmStatic
        fun mpsToMiph(n: Double): Double {
            return n * MpS_TO_MIpH
        }

        /**
         * convert km/h to mi/h
         */
        @JvmStatic
        fun kmphToMiph(n: Double): Double {
            return n * KM_TO_MI
        }

        /**
         * convert min/km to min/mi (multiply with a corresponding constant)
         */
        @JvmStatic
        fun minpkmToMinpmi(n: Double): Double {
            return n * MINpKM_TO_MINpMI
        }

        /**
         * convert min/mi to min/km (multiply with a corresponding constant)
         */
        @JvmStatic
        fun minpmiToMinpkm(n: Double): Double {
            return n / MINpKM_TO_MINpMI
        }
    }

}