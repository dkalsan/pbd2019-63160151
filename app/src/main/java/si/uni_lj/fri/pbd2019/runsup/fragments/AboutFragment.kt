package si.uni_lj.fri.pbd2019.runsup.fragments

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.util.Log
import android.view.*
import kotlinx.android.synthetic.main.fragment_about.*
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.R
import si.uni_lj.fri.pbd2019.runsup.model.User
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService

class AboutFragment: Fragment() {

    private lateinit var databaseHelper: DatabaseHelper

    companion object {
        fun newInstance(): AboutFragment {
            return AboutFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        // Initialize db helper
        databaseHelper = DatabaseHelper(activity.applicationContext)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity.title = getString(R.string.menu_about)
        return inflater?.inflate(R.layout.fragment_about, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.findItem(R.id.action_delete)?.isVisible = false
        menu?.findItem(R.id.action_clearhistory)?.isVisible = false
    }

}