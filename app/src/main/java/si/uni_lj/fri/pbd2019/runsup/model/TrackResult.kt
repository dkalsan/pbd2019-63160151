package si.uni_lj.fri.pbd2019.runsup.model

class TrackResult {
    var trackId: String? = null
    var personalBest: Long? = 0
    var rating: Float? = null

    constructor() {
        // Needed for Firebase
    }

    constructor(trackId: String?, personalBest: Long?) {
        this.trackId = trackId
        this.personalBest = personalBest
    }
}