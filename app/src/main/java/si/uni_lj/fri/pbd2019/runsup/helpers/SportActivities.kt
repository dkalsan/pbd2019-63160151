package si.uni_lj.fri.pbd2019.runsup.helpers

import android.util.Log
import kotlin.math.ceil

class SportActivities {
    companion object {


        const val RUNNING: Int = 0
        const val WALKING: Int = 1
        const val CYCLING: Int = 2

        /**
         * Default weight of the user in kilograms
         */
        const val DEFAULT_WEIGHT = 60f

        /**
         * Default constants for conversion from speed to MET if it cannot be found in the speedToMetTable
         *  0 - running,
         *  1 - walking,
         *  2 - cycling
         */
        private val avgMetValue = listOf(
            1.535353535,
            1.14,
            0.744444444
        )

        /**
         *  Table provides the conversions from speed to MET for a given sport activity
         *  0 - running,
         *  1 - walking,
         *  2 - cycling
         */
        private val speedToMetTable = listOf(
            mapOf(
                4 to 6.0,
                5 to 8.3,
                6 to 9.86,
                7 to 11.0,
                8 to 11.8,
                9 to 12.8,
                10 to 14.5,
                11 to 16.0,
                12 to 19.0,
                13 to 19.8,
                14 to 23.0
            ),
            mapOf(
                1 to 2.0,
                2 to 2.8,
                3 to 3.1,
                4 to 3.5
            ),
            mapOf(
                10 to 6.8,
                12 to 8.0,
                14 to 10.0,
                16 to 12.8,
                18 to 13.6,
                20 to 15.8
            )
        )

        /**
         * Returns MET value for an activity.
         * @param activityType - sport activity type (0 - running, 1 - walking, 2 - cycling)
         * @param speed - speed in m/s
         * @return
         */
        @JvmStatic
        fun getMET(activityType: Int, speed: Float): Double {
            return speedToMetTable[activityType][ceil(MainHelper.mpsToMiph(speed.toDouble())).toInt()] ?: MainHelper.mpsToMiph(speed.toDouble()) * avgMetValue[activityType]
        }

        /**
         * Returns final calories computed from the data provided (returns value in kcal)
         * @param sportActivity - sport activity type (0 - running, 1 - walking, 2 - cycling)
         * @param weight - weight in kg
         * @param speedList - list of all speed values recorded (unit = m/s)
         * @param timeFillingSpeedListInHours - time of collecting speed list (duration of sport activity from first to last speedPoint in speedList)
         * @return
         */
        @JvmStatic
        fun countCalories(sportActivity: Int, weight: Float, speedList: List<Float>, timeFillingSpeedListInHours: Double): Double {
            return getMET(sportActivity, speedList.average().toFloat()) * weight * timeFillingSpeedListInHours
        }
    }
}

