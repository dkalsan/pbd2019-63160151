package si.uni_lj.fri.pbd2019.runsup.fragments

import android.content.*
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityCompat.invalidateOptionsMenu
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.*
import android.widget.Button
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper
import si.uni_lj.fri.pbd2019.runsup.services.TrackerService
import kotlinx.android.synthetic.main.fragment_stopwatch.view.*
import si.uni_lj.fri.pbd2019.runsup.ActiveWorkoutMapActivity
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.R
import si.uni_lj.fri.pbd2019.runsup.WorkoutDetailActivity
import si.uni_lj.fri.pbd2019.runsup.model.Workout
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper
import si.uni_lj.fri.pbd2019.runsup.settings.SettingsActivity

class StopwatchFragment: Fragment() {

    private val STOP_TAG = "stop"
    private val START_TAG = "start"
    private val CONTINUE_TAG = "continue"
    private val RC_LOCATION_BTNSTART = 20
    private val RC_LOCATION_BTNCONTINUE = 21
    private val RC_LOCATION_ONCREATE = 30

    private var localState = Constants.STATE_NEVER_STARTED

    lateinit var mBroadcastReceiver: BroadcastReceiver
    private lateinit var databaseHelper: DatabaseHelper

    // Workout details values
    private var sportActivity: Int = 0
    private var duration: Long = 0
    private var distance: Double = 0.0
    private var calories: Double = 0.0
    private var pace: Double = 0.0
    private var serviceState: Int = -1
    private var workoutId: Long = -1
    private var unit: String = ""
    private lateinit var workoutPositionList: ArrayList<List<Location>>
    private lateinit var tmpPositionArrayList: ArrayList<Location>
    private lateinit var prefs: SharedPreferences
    private lateinit var prefsListener: SharedPreferences.OnSharedPreferenceChangeListener

    companion object {
        fun newInstance(): StopwatchFragment {
            return StopwatchFragment()
        }
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        // Initialize database helper
        databaseHelper = DatabaseHelper(activity.applicationContext)

        /*
        val workoutDao = databaseHelper.workoutDao()
        TableUtils.dropTable(workoutDao, false)
        TableUtils.createTable(workoutDao)
        */

        // TMP >>>
        /*
        val workoutDao = databaseHelper.workoutDao()
        val workout = Workout("Test TESTETES", 1)
        workout.distance = 1752.0
        workout.duration = 700L
        workout.totalCalories = 1432.0
        workout.sportActivity = 2
        workout.status = Workout.statusPaused
        workoutDao.create(workout)

        val gpsPointDao = databaseHelper.gpsPointDao()
        val g1 = GpsPoint(
            workout,
            0,
            Location(LocationManager.GPS_PROVIDER).apply {
                latitude = 46.020414
                longitude = 14.596320
                elapsedRealtimeNanos = 10000L
            },
            5L,
            6.0f,
            0.0,
            120.0
        )
        gpsPointDao.create(g1)
        val g2 = GpsPoint(
            workout,
            0,
            Location(LocationManager.GPS_PROVIDER).apply {
                latitude = 46.020753
                longitude = 14.596519
                elapsedRealtimeNanos = 200000L
            },
            6L,
            6.0f,
            0.0,
            120.0
        )
        gpsPointDao.create(g2)
        val g3 = GpsPoint(
            workout,
            1,
            Location(LocationManager.GPS_PROVIDER).apply {
                latitude = 46.020987
                longitude = 14.596765
                elapsedRealtimeNanos = 3000000L
            },
            7L,
            6.0f,
            0.0,
            120.0
        )
        gpsPointDao.create(g3)
        */
        // <<<

        // Initialize tmpPositionArrayList
        tmpPositionArrayList = arrayListOf()

        // Initialize broadcast receiver
        mBroadcastReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {

                if(intent?.action == Constants.BROADCAST_TRACKER_DETAILS) {

                    // Extract detail values
                    duration = intent.getLongExtra("duration", -1)
                    distance = intent.getDoubleExtra("distance", -1.0)
                    pace = intent.getDoubleExtra("pace", -1.0)
                    calories = intent.getDoubleExtra("calories", -1.0)
                    serviceState = intent.getIntExtra("state", -1)
                    sportActivity = intent.getIntExtra("sportActivity", -1)
                    workoutId = intent.getLongExtra("workoutId", -1L)


                    if(unit == "1") {
                        pace = MainHelper.minpkmToMinpmi(pace)
                        distance = MainHelper.kmToMi(distance)
                    }

                    // Assign detail values to views
                    assignDetailsToViews()
                }
            }
        }

        // Start listening to the TrackerService
        startReceivingBroadcast()

        // Initializing position list of all GPS positions
        workoutPositionList = arrayListOf()

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater!!.inflate(R.layout.fragment_stopwatch, container, false)

        // Set the fragment title
        activity.title = getString(R.string.menu_workout)

        // As the name suggests!
        handlePermissionChecking(RC_LOCATION_ONCREATE)

        // Set up the button appropriately to the state before configuration change
        if (savedInstanceState != null) {
            with(savedInstanceState) {
                getString("buttonState").also { tag ->
                    view.button_stopwatch_start.tag = tag
                    when(tag) {
                        STOP_TAG -> view.button_stopwatch_start.text = getString(R.string.stopwatch_stop)
                        START_TAG -> view.button_stopwatch_start.text = getString(R.string.stopwatch_start)
                        CONTINUE_TAG -> {
                            view.button_stopwatch_start.text = getString(R.string.stopwatch_continue)
                            view.button_stopwatch_endworkout.visibility = View.VISIBLE
                        }
                    }
                }

                /*
                // TODO: I think I dont need this anymore since the views are being set onresume with fetching form db
                view.textview_stopwatch_duration.text = getString("duration")
                view.textview_stopwatch_distance.text = getString("distance")
                view.textview_stopwatch_pace.text = getString("pace")
                view.textview_stopwatch_calories.text = getString("calories")
                */

                sportActivity = getInt("sportActivity")
                localState = getInt("localState")
                view.button_stopwatch_selectsport.text = resources.getStringArray(R.array.dialog_sport_list_values)[sportActivity]
            }
        }

        // OnClick listener for ending the workout
        view.button_stopwatch_endworkout.setOnClickListener {
            val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
            builder.setTitle(R.string.dialog_title)
            builder.setMessage(R.string.dialog_message)
            builder.setPositiveButton(R.string.yes) { _, _ ->

                // Stop the service
                Intent(activity, TrackerService::class.java).also { intent ->
                    intent.action = Constants.COMMAND_STOP
                    intent.putExtra("sportActivity", sportActivity)
                    activity.startService(intent)
                }

                // Send an intent for workout details
                Intent(activity, WorkoutDetailActivity::class.java).apply {
                    putExtra("workoutId", workoutId)
                    activity.startActivity(this)
                }

            }
            builder.setNegativeButton(R.string.no, null)
            builder.create().show()
        }

        // OnClick listener for starting, stopping/pausing and continuing the workout
        view.button_stopwatch_start.setOnClickListener {

            val btn = it as Button
            when(btn.tag) {

                START_TAG -> {
                    handlePermissionChecking(RC_LOCATION_BTNSTART)
                }
                CONTINUE_TAG -> {
                    handlePermissionChecking(RC_LOCATION_BTNCONTINUE)
                }
                STOP_TAG -> {

                    setupViewsOnBtnStopClick(true)

                    // Save the current temporary list of locations to the main list
                    workoutPositionList.add(tmpPositionArrayList.toList())


                }
            }

        }

        // OnClick listener for changing the workout type
        view.button_stopwatch_selectsport.setOnClickListener {
            val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
            builder.setTitle(R.string.dialog_title_sportselection)

            builder.setItems(R.array.dialog_sport_list_values) { _, selectedActivity ->

                // Update button and save the sportActivity
                sportActivity = selectedActivity
                view.button_stopwatch_selectsport.text = resources.getStringArray(R.array.dialog_sport_list_values)[selectedActivity]

                // Send an intent to the service to update the sport activity
                Intent(activity, TrackerService::class.java).apply {
                    putExtra("sportActivity", sportActivity)
                    putExtra("workoutId", workoutId)
                    action = Constants.UPDATE_SPORT_ACTIVITY
                    activity.startService(this)
                }
            }

            builder.setNegativeButton(R.string.cancel, null)
            builder.create().show()
        }

        // OnClick listener to open the active workout map
        view.button_stopwatch_activeworkout.setOnClickListener {
            Intent(activity, ActiveWorkoutMapActivity::class.java).apply {
                putExtra("state", localState)
                putExtra("workoutId", workoutId)
                startActivity(this)
            }
        }

        return view
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            RC_LOCATION_BTNSTART -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    setupViewsOnBtnStartClick(true)
                }
            }
            RC_LOCATION_BTNCONTINUE -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    setupViewsOnBtnContinueClick(true)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        startReceivingBroadcast()

        getLastWorkoutFromDb()

        // Get unit
        unit = activity.getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
            .getString(Constants.SAVED_UNIT_KEY, "0")

        // Reset view values
        initializeDetailsToViews()

        // Start the service without a command

        Intent(activity, TrackerService::class.java).apply {
            putExtra("sportActivity", sportActivity)
            activity.startService(this)
        }
    }

    public override fun onPause() {
        super.onPause()

        stopReceivingBroadcast()

        if(!activity.isChangingConfigurations && localState != Constants.STATE_CONTINUE && localState != Constants.STATE_RUNNING) {
            activity.stopService(Intent(activity, TrackerService::class.java))
        }

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putString("buttonState", view?.button_stopwatch_start?.tag as String)
            putString("duration", view?.textview_stopwatch_duration?.text as String)
            putString("distance", view?.textview_stopwatch_distance?.text as String)
            putString("pace", view?.textview_stopwatch_pace?.text as String)
            putString("calories", view?.textview_stopwatch_calories?.text as String)
            putInt("sportActivity", sportActivity)
            putInt("localState", localState)
        }

        super.onSaveInstanceState(outState)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.findItem(R.id.action_delete)?.isVisible = false
        menu?.findItem(R.id.action_clearhistory)?.isVisible = false
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onStart() {
        super.onStart()

    }

    private fun initializeDetailsToViews() {
        if(unit == "1") {
            distance = MainHelper.kmToMi(distance)
            pace = MainHelper.minpkmToMinpmi(pace)
            view?.textview_stopwatch_distanceunit?.text = activity.getString(R.string.all_labeldistanceunitmiles)
            view?.textview_stopwatch_unitpace?.text = getString(R.string.unit_pace, getString(R.string.all_min), getString(R.string.all_labeldistanceunitmiles))
        } else {
            view?.textview_stopwatch_distanceunit?.text = activity.getString(R.string.all_labeldistanceunitkilometers)
            view?.textview_stopwatch_unitpace?.text = getString(R.string.unit_pace, getString(R.string.all_min), getString(R.string.all_labeldistanceunitkilometers))
        }
        view?.textview_stopwatch_pace?.text = MainHelper.formatPace(pace)
        view?.textview_stopwatch_distance?.text = MainHelper.formatDistance(distance)
    }

    private fun assignDetailsToViews() {

        // Assign values to the corresponding views
        view?.textview_stopwatch_duration?.text = MainHelper.formatDuration(duration)
        view?.textview_stopwatch_distance?.text = MainHelper.formatDistance(distance)
        view?.textview_stopwatch_pace?.text = MainHelper.formatPace(pace)
        view?.textview_stopwatch_calories?.text = MainHelper.formatCalories(calories)
        view?.button_stopwatch_selectsport?.text = resources.getStringArray(R.array.dialog_sport_list_values)[sportActivity]
    }

    private fun resetButtons() {
        view?.button_stopwatch_start?.text = getString(R.string.stopwatch_start)
        view?.button_stopwatch_start?.tag = START_TAG
        view?.button_stopwatch_endworkout?.visibility = View.GONE
    }

    private fun resetDetails() {
        duration = 0L
        distance = 0.0
        calories = 0.0
        pace = 0.0
    }

    private fun startReceivingBroadcast() {
        LocalBroadcastManager.getInstance(activity).registerReceiver(
            mBroadcastReceiver,
            IntentFilter(Constants.BROADCAST_TRACKER_DETAILS)
        )
    }

    private fun stopReceivingBroadcast() {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(
            mBroadcastReceiver
        )
    }

    private fun handlePermissionChecking(requestCode: Int) {

        // Check whether the user allows us to ask him for GPS permissions
        if(activity.getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE)
                .getBoolean(Constants.SAVED_GPSCHECK_KEY, true)) {

            // Check permissions
            if(ActivityCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    requestCode
                )
            } else {
                when(requestCode) {
                    RC_LOCATION_BTNSTART -> setupViewsOnBtnStartClick(true)
                    RC_LOCATION_BTNCONTINUE -> setupViewsOnBtnContinueClick(true)
                }
            }

        } else {
            val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
            builder.setTitle(getString(R.string.dialog_title_gpslocation))
            builder.setMessage(getString(R.string.dialog_message_gpslocation))
            builder.setPositiveButton(getString(R.string.dialog_positive_gpslocation)) { _, _ ->
                Intent(activity, SettingsActivity::class.java).also {
                    startActivity(it)
                }
            }
            builder.setNegativeButton(getString(R.string.cancel), null)
            builder.create().show()

            when(requestCode) {
                RC_LOCATION_BTNSTART -> setupViewsOnBtnStartClick(true)
                RC_LOCATION_BTNCONTINUE -> setupViewsOnBtnContinueClick(true)
            }
        }

    }

    private fun setupViewsOnBtnContinueClick(startIntent: Boolean) {

        // Setup button and state values
        val btn = view?.button_stopwatch_start
        btn?.text = getString(R.string.stopwatch_stop)
        btn?.tag = STOP_TAG
        view?.button_stopwatch_endworkout?.visibility = View.GONE
        localState = Constants.STATE_CONTINUE

        // Start service - Continue workout
        if(startIntent) {
            Intent(activity, TrackerService::class.java).apply {
                putExtra("sportActivity", sportActivity)
                action = Constants.COMMAND_CONTINUE
                activity.startService(this)
            }
        }

    }

    private fun setupViewsOnBtnStartClick(startIntent: Boolean) {

        // Setup button and state values
        val btn = view?.button_stopwatch_start
        btn?.text = getString(R.string.stopwatch_stop)
        btn?.tag = STOP_TAG
        localState = Constants.STATE_RUNNING

        // Start service - Start workout
        if(startIntent) {
            Intent(activity, TrackerService::class.java).apply {
                putExtra("sportActivity", sportActivity)
                action = Constants.COMMAND_START
                activity.startService(this)
            }
        }

    }

    private fun setupViewsOnBtnStopClick(startIntent: Boolean) {

        val btn = view?.button_stopwatch_start
        btn?.text = getString(R.string.stopwatch_continue)
        btn?.tag = CONTINUE_TAG
        view?.button_stopwatch_endworkout?.visibility = View.VISIBLE
        localState = Constants.STATE_PAUSED


        // Start service - Pause workout
        if(startIntent) {
            Intent(activity, TrackerService::class.java).apply {
                putExtra("sportActivity", sportActivity)
                action = Constants.COMMAND_PAUSE
                activity.startService(this)
            }
        }

    }

    private fun getLastWorkoutFromDb() {

        val workoutDao = databaseHelper.workoutDao()

        val q = workoutDao.query(
                            workoutDao
                                .queryBuilder()
                                .orderBy("id", false)
                                .limit(1L)
                                .prepare()
        )

        if(q.isNotEmpty()) {
            val lastWorkout = q.first()
            if(lastWorkout.status == Workout.statusPaused || lastWorkout.status == Workout.statusUnknown) {

                val gpsDao = databaseHelper.gpsPointDao()
                val qGps = gpsDao.queryForEq("workout_id", lastWorkout)

                // Extract values
                duration = lastWorkout.duration ?: 0L
                distance = lastWorkout.distance ?: 0.0
                pace = if (qGps.isNotEmpty()) qGps.last().pace else 0.0
                calories = lastWorkout.totalCalories ?: 0.0
                sportActivity = lastWorkout.sportActivity ?: 0
                workoutId = lastWorkout.id

                // Tell the service to load the previous workout
                Intent(activity, TrackerService::class.java).apply {
                    putExtra("workoutId", workoutId)
                    action = Constants.LOAD_WORKOUT_BY_ID
                    activity.startService(this)
                }

                when(lastWorkout.status) {
                    Workout.statusPaused -> setupViewsOnBtnStopClick(false)
                    Workout.statusUnknown -> setupViewsOnBtnStartClick(false)
                }

                // TODO: Use workout object instead of all the fields

                assignDetailsToViews()
            } else {
                resetButtons()
                resetDetails()
                assignDetailsToViews()

            }
        } else {
            assignDetailsToViews()
        }

    }

    /*
    private fun clearWorkoutsFromDb() {
        val workoutDao = databaseHelper.workoutDao()
        TableUtils.dropTable(workoutDao, false)
        TableUtils.createTable(workoutDao)
    }
    */
}