package si.uni_lj.fri.pbd2019.runsup.model

import android.net.Uri

class Competitor {
    var displayName: String? = null
    var photoUrl: String? = null
    var lat: Double = 0.0
    var lng: Double = 0.0
    var trackResults: ArrayList<TrackResult>? = null

    constructor() {
        // Needed for Firebase
    }

    constructor(displayName: String?, photoUrl: String?) {
        this.displayName = displayName
        this.photoUrl = photoUrl
        this.trackResults = arrayListOf()
    }
}