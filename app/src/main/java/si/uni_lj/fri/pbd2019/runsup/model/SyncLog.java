package si.uni_lj.fri.pbd2019.runsup.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "SyncLog")
public class SyncLog {

    /**
     * Constructors
     */

    public SyncLog() {
        // Required by ORMLite
    }

    /**
     * Model variables
     */

    @DatabaseField(generatedId = true)
    private Long id;

    /**
     * Getters and setters
     */

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
