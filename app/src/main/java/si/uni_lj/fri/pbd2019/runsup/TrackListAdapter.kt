package si.uni_lj.fri.pbd2019.runsup

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.PolyUtil
import kotlinx.android.synthetic.main.adapter_compete.view.*
import si.uni_lj.fri.pbd2019.runsup.model.Track

class TrackListAdapter(private val ctx: Context, private val resource: Int, private val trackList: ArrayList<Track>): ArrayAdapter<Track>(ctx, resource, trackList) {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return trackList.size
    }

    override fun getItem(position: Int): Track {
        return trackList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder

        if(convertView == null) {
            view = inflater.inflate(R.layout.adapter_compete, parent, false)

            holder = ViewHolder()
            holder.titleTv = view.textview_compete_title
            holder.locationTv = view.textview_compete_location
            holder.difficultyTv = view.textview_compete_difficulty
            holder.mapIv = view.imageview_compete_map

            view.tag = holder
        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        val track = trackList[position]

        // Set title
        view.textview_compete_title.text = track.title

        // Set location
        view.textview_compete_location.text = track.location

        // Set difficulty
        view.textview_compete_difficulty.text = track.difficulty

        // Get the map from Google Static Maps API
        val qLocationLatLng = trackList[position].geopoints.map { LatLng(it.lat, it.lng) }
        val bounds = qLocationLatLng
            .fold(LatLngBounds.Builder(), LatLngBounds.Builder::include)
            .build()
        val center = bounds.center

        val queryUrl = "https://maps.googleapis.com/maps/api/staticmap" +
                "?size=400x400&center=${center.latitude},${center.longitude}&zoom=15" +
                "&path=weight:3%7Ccolor:red%7Cenc:${PolyUtil.encode(qLocationLatLng)}" +
                "&key=${ctx.getString(R.string.google_maps_static_key)}"

        Glide
            .with(ctx)
            .load(queryUrl)
            .asBitmap()
            .placeholder(R.drawable.baseline_map_black_48)
            .into(view.imageview_compete_map)

        return view
    }

    private class ViewHolder {
        lateinit var titleTv: TextView
        lateinit var locationTv: TextView
        lateinit var difficultyTv: TextView
        lateinit var mapIv: ImageView
    }

}