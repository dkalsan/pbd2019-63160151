package si.uni_lj.fri.pbd2019.runsup

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import si.uni_lj.fri.pbd2019.runsup.helpers.MapHelper


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapLoadedCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        setupActionBar()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragment_maps_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMapLoadedCallback(this)
        mMap.uiSettings.isZoomControlsEnabled = true

    }

    override fun onMapLoaded() {
        val workoutId = intent.getLongExtra("workoutId", -1)

        if(workoutId != -1L) {
            // Called to display by workoutId
            MapHelper.setUpMap(mMap, intent.getLongExtra("workoutId", -1), this, true)
        } else {
            // Called to display by trackId
            MapHelper.setUpMap(mMap, intent.getStringExtra("trackId"), this)
        }
    }

    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }
}
