package si.uni_lj.fri.pbd2019.runsup.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.R
import si.uni_lj.fri.pbd2019.runsup.model.Track
import kotlinx.android.synthetic.main.fragment_compete.view.*
import si.uni_lj.fri.pbd2019.runsup.ChallengeActivity
import si.uni_lj.fri.pbd2019.runsup.TrackListAdapter

class TrackSelectionFragment: Fragment() {

    private lateinit var mDatabase: DatabaseReference
    private lateinit var auth: FirebaseAuth
    private lateinit var trackList: ArrayList<Track>
    private lateinit var trackListKeys: ArrayList<String>
    private var activityContext: Context? = null

    companion object {
        fun newInstance(): TrackSelectionFragment {
            return TrackSelectionFragment()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        // Get database connection
        mDatabase = FirebaseDatabase.getInstance().reference

        // Set a listener for Firebase Auth changes
        auth = FirebaseAuth.getInstance()
        auth.addAuthStateListener {
            updateUI(auth.currentUser)
        }

        // Init tracklist
        trackList = arrayListOf()
        trackListKeys = arrayListOf()
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_compete, container, false)
        activity.title = getString(R.string.menu_compete)

        view.listview_compete_tracks.setOnItemClickListener { parent, view, position, id ->
            Intent(activity, ChallengeActivity::class.java).apply {
                putExtra("trackId", trackListKeys[position])
                activity.startActivity(this)
            }
        }

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        menu?.findItem(R.id.action_delete)?.isVisible = false
        menu?.findItem(R.id.action_clearhistory)?.isVisible = false
        menu?.findItem(R.id.action_sync)?.isVisible = false
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activityContext = context
    }

    override fun onDetach() {
        super.onDetach()
        activityContext = null
    }

    private fun updateUI(user: FirebaseUser?) {

        if(user != null) {

            // Set views appropriately
            view?.textview_compete_loggedout?.visibility = View.GONE
            view?.progressbar_compete_loadingtracks?.visibility = View.VISIBLE
            view?.listview_compete_tracks?.visibility = View.VISIBLE

            // Get all tracks
            mDatabase
                .child(Constants.FIREBASE_TRACKS_KEY)
                .addValueEventListener(object: ValueEventListener {
                    override fun onCancelled(error: DatabaseError) = Unit
                    override fun onDataChange(snapshot: DataSnapshot) {

                        // Show user that we're loading new data
                        view?.progressbar_compete_loadingtracks?.visibility = View.VISIBLE

                        trackList.clear()
                        trackListKeys.clear()

                        snapshot.children.forEach {
                            val track = it.getValue(Track::class.java)
                            if(track != null) {
                                trackList.add(track)
                                trackListKeys.add(it.key!!)
                            }
                        }

                        // Handling possible error due to activity context not being available (transaction not firing yet)
                        if(activityContext != null) {
                            val adapter = TrackListAdapter(activity, android.R.layout.simple_list_item_1, trackList)
                            view?.listview_compete_tracks?.adapter = adapter

                            view?.progressbar_compete_loadingtracks?.visibility = View.GONE
                        }
                    }
                })
        } else {

            // Set views appropriately
            view?.textview_compete_loggedout?.visibility = View.VISIBLE
            view?.progressbar_compete_loadingtracks?.visibility = View.GONE
            view?.listview_compete_tracks?.visibility = View.GONE

        }
    }
}