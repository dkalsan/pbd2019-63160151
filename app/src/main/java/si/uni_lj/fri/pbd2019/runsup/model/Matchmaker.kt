package si.uni_lj.fri.pbd2019.runsup.model

class Matchmaker {

    var competitionId: String? = null
    var initiatorUid: String? = null
    var trackId: String? = null


    constructor() {
        // Needed for Firebase
    }

    constructor(competitionId: String?, initiatorUid: String?, trackId: String?) {
        this.competitionId = competitionId
        this.initiatorUid = initiatorUid
        this.trackId = trackId
    }
}