package si.uni_lj.fri.pbd2019.runsup.model

import android.text.BoringLayout

class Competition {

    var competitorA: Competitor? = null
    var competitorB: Competitor? = null
    var trackId: String? = null
    var startTimeA: Long? = null
    var startTimeB: Long? = null
    var endTimeA: Long? = null
    var endTimeB: Long? = null
    var forfeited: Boolean = false

    constructor() {
        // Needed for Firebase
    }

    constructor(trackId: String?, competitorA: Competitor?) {
        this.trackId = trackId
        this.competitorA = competitorA
    }
}