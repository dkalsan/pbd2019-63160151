package si.uni_lj.fri.pbd2019.runsup

class Constants {
    companion object {
        // Commands
        const val COMMAND_START = "si.uni_lj.fri.pbd2019.runsup.COMMAND_START"
        const val COMMAND_CONTINUE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_CONTINUE"
        const val COMMAND_PAUSE = "si.uni_lj.fri.pbd2019.runsup.COMMAND_PAUSE"
        const val COMMAND_STOP = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STOP"
        const val COMMAND_STATUS = "si.uni_lj.fri.pbd2019.runsup.COMMAND_STATUS"

        // Update commands
        const val UPDATE_SPORT_ACTIVITY = "si.uni_lj.fri.pdb2019.runsup.UPDATE_SPORT_ACTIVITY"

        // Load commands
        const val LOAD_WORKOUT_BY_ID = "si.uni_lj.fri.pdb2019.runsup.LOAD_WORKOUT_BY_ID"
        const val LOAD_TRACK_BY_ID = "si.uni_lj.fri.pdb2019.runsup.LOAD_TRACK_BY_ID"

        // Broadcasts
        const val BROADCAST_TRACKER_DETAILS = "si.uni_lj.fri.pbd2019.runsup.TICK"
        const val BROADCAST_CHALLENGE_DETAILS = "si.uni_lj.fri.pbd2019.runsup.LOCATION_RECEIVED"
        const val BROADCAST_CHALLENGE_STATUS = "si.uni_lj.fri.pbd2019.runsup.STATUS"

        // States
        const val STATE_NEVER_STARTED = -1
        const val STATE_STOPPED = 0
        const val STATE_RUNNING = 1
        const val STATE_PAUSED = 2
        const val STATE_CONTINUE = 3

        // Shared preferences file names
        const val DEFAULT_SHARED_PREFERENCES = "si.uni_lj.fri.pbd2019.runsup_preferences"

        // Shared preferences keys
        const val SAVED_UNIT_KEY = "unit"
        const val SAVED_USERID_KEY = "userId"
        const val SAVED_LOGINSTATE_KEY = "userSignedIn"
        const val SAVED_GPSCHECK_KEY = "gpsCheck"
        const val SAVED_WEIGHT_KEY = "weight"

        // Firease keys
        const val FIREBASE_TRACKS_KEY = "tracks"
        const val FIREBASE_COMPETITORS_KEY = "competitors"
        const val FIREBASE_COMPETITIONS_KEY = "competitions"
        const val FIREBASE_MATCHMAKERS_KEY = "matchmakers"
    }
}