package si.uni_lj.fri.pbd2019.runsup

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.ui.IconGenerator
import kotlinx.android.synthetic.main.activity_active_workout_map.*
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper
import si.uni_lj.fri.pbd2019.runsup.helpers.MapHelper
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper

class ActiveWorkoutMapActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, GoogleMap.OnCameraMoveCanceledListener {

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private var USER_INTERACTION_FLAG = false
    }

    private lateinit var mMap: GoogleMap
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var databaseHelper: DatabaseHelper
    private lateinit var drawerHandler: Handler
    private lateinit var drawerRunnable: Runnable
    lateinit var mBroadcastReceiver: BroadcastReceiver

    private lateinit var temporaryPositionList: ArrayList<LatLng>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_active_workout_map)
        setupActionBar()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.fragment_activeworkoutmap_map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Init db helper
        databaseHelper = DatabaseHelper(this)

        // Init onclick listener
        button_activeworkoutmap_back.setOnClickListener {
            Intent(this, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(this)
                finish()
            }
        }

        // Initialize broadcast receiver
        mBroadcastReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {

                if(intent?.action == Constants.BROADCAST_TRACKER_DETAILS) {

                    // Extract latest location
                    lastLocation = intent.getParcelableExtra("location")

                    val currentLatLng = LatLng(lastLocation.latitude, lastLocation.longitude)

                    if(!USER_INTERACTION_FLAG) {
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 16f))
                    }

                    temporaryPositionList.add(LatLng(lastLocation.latitude, lastLocation.longitude))
                }

            }
        }

        // Init temporary position list
        temporaryPositionList = arrayListOf()

        // Initialize drawer runnable
        createDrawerRunnable()

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setOnMapLoadedCallback(this)
        mMap.setOnCameraMoveCanceledListener(this)
        mMap.uiSettings.isZoomControlsEnabled = true
    }

    override fun onMapLoaded() {

        setUpMap()

        // Draw initial map
        MapHelper.setUpMap(mMap, intent.getLongExtra("workoutId", -1), this, false)

        // start runnable if the workout is running
        if(intent.getIntExtra("state", -1) != Constants.STATE_PAUSED) {
            drawerRunnable.run()
        } else {
            drawPauseIcon()
        }

    }

    override fun onCameraMoveCanceled() {
        USER_INTERACTION_FLAG = true
    }

    override fun onPause() {
        super.onPause()
        stopReceivingBroadcast()
    }

    private fun setUpMap() {

        // Check for location permission
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                ActiveWorkoutMapActivity.LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        // Enables the my-location layer
        mMap.isMyLocationEnabled = true

        /// Start listening to the TrackerService
        startReceivingBroadcast()

    }

    private fun startReceivingBroadcast() {
        LocalBroadcastManager.getInstance(this).registerReceiver(
            mBroadcastReceiver,
            IntentFilter(Constants.BROADCAST_TRACKER_DETAILS)
        )
    }

    private fun stopReceivingBroadcast() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
            mBroadcastReceiver
        )
    }

    private fun createDrawerRunnable() {
        drawerHandler = Handler()
        drawerRunnable = object : Runnable {
            override fun run() {
                try{
                    // Draw
                    mMap.addPolyline(
                        PolylineOptions()
                            .addAll(
                                temporaryPositionList
                            )
                    )

                    // Empty list
                    if(temporaryPositionList.isNotEmpty()) {
                        val tmp = temporaryPositionList.last()
                        temporaryPositionList.clear()
                        temporaryPositionList.add(tmp)
                    }

                } finally {
                    drawerHandler.postDelayed(this, 15000)
                }
            }
        }
    }

    private fun drawPauseIcon() {
        val gpsPointDao = databaseHelper.gpsPointDao()

        val q = gpsPointDao.queryForEq("workout_id", intent.getLongExtra("workoutId", -1))

        if(q.isNotEmpty()) {
            val sessionNbr = q.maxBy { it.sessionNumber }?.sessionNumber ?: 0
            val lastGpsPoint = q.last()

            val icg = IconGenerator(this)
            icg.setTextAppearance(R.style.TextAppearance_AppCompat_Button)
            icg.setColor(Color.YELLOW)
            val pauseIcon = icg.makeIcon("Pause ${sessionNbr + 1}")

            mMap.addMarker(
                MarkerOptions()
                    .position(LatLng(lastGpsPoint.latitude, lastGpsPoint.longitude)))
                .setIcon(BitmapDescriptorFactory.fromBitmap(pauseIcon))
        }
    }

    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

}
