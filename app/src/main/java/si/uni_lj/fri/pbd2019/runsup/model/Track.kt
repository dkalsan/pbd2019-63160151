package si.uni_lj.fri.pbd2019.runsup.model

import com.google.firebase.database.IgnoreExtraProperties


@IgnoreExtraProperties
class Track {
    var title: String? = null
    var location: String? = null
    var difficulty: String? = null
    var rating: Float = 0.0f
    var ratingCounter: Int = 0
    var bestTime: Long = 0L
    var geopoints: ArrayList<Geopoint> = arrayListOf()

    constructor() {
        // Needed for Firebase
    }

    constructor(title: String?, location: String?, difficulty: String, geopoints: ArrayList<Geopoint>) {
        this.title = title
        this.location = location
        this.difficulty = difficulty
        this.geopoints = geopoints
        this.rating = 0.0f
        this.ratingCounter = 0
        this.bestTime = 0
    }
}