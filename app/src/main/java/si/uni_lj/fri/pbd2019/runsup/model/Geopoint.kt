package si.uni_lj.fri.pbd2019.runsup.model

class Geopoint {
    var seqId: Int = 0
    var lat: Double = 0.0
    var lng: Double = 0.0

    constructor(){
        // Needed for Firebase
    }

    constructor(seqId: Int, lat: Double, lng: Double) {
        this.seqId = seqId
        this.lat = lat
        this.lng = lng
    }
}