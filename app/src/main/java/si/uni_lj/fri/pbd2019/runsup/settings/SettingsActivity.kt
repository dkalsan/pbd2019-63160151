package si.uni_lj.fri.pbd2019.runsup.settings

import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.preference.*
import android.util.Log
import android.view.MenuItem
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.MainActivity
import si.uni_lj.fri.pbd2019.runsup.R

/**
 * A [PreferenceActivity] that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 *
 * See [Android Design: Settings](http://developer.android.com/design/patterns/settings.html)
 * for design guidelines and the [Settings API Guide](http://developer.android.com/guide/topics/ui/settings.html)
 * for more information on developing a Settings UI.
 */
class SettingsActivity : AppCompatPreferenceActivity() {

    init {
        instance = this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActionBar()

        fragmentManager
            .beginTransaction()
            .replace(android.R.id.content,
                MainPreferenceFragment()
            )
            .commit()
    }

    /**
     * Set up the [android.app.ActionBar], if the API is available.
     */
    private fun setupActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * {@inheritDoc}
     */
    override fun onIsMultiPane(): Boolean {
        return isXLargeTablet(this)
    }

    /**
     * This method stops fragment injection in malicious applications.
     * Make sure to deny any unknown fragments here.
     */
    override fun isValidFragment(fragmentName: String): Boolean {
        return MainPreferenceFragment::class.java.name == fragmentName
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    class MainPreferenceFragment : PreferenceFragment() {

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.pref_main)
            setHasOptionsMenu(true)

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.

            bindPreferenceSummaryToValue(findPreference(Constants.SAVED_UNIT_KEY))
            handleCheckBoxPreferenceOnChangeListener(findPreference(Constants.SAVED_GPSCHECK_KEY))
        }

    }

    companion object {

        private var instance: SettingsActivity? = null

        private fun getActivityContext(): Context {
            return instance!!.applicationContext
        }

        private fun savePreferenceValueToSharedPrefs(preference: Preference, value: Any) {

            if(value is Boolean) {
                PreferenceManager.getDefaultSharedPreferences(getActivityContext())
                    .edit()
                    .putBoolean(preference.key, value)
                    .apply()
            } else if(value is String) {
                PreferenceManager.getDefaultSharedPreferences(getActivityContext())
                    .edit()
                    .putString(preference.key, value)
                    .apply()
            }

        }

        /**
         * A preference value change listener that updates the preference's summary
         * to reflect its new value.
         */
        private val sBindPreferenceSummaryToValueListenerAndSaveToSharedPrefs = Preference.OnPreferenceChangeListener { preference, value ->
            val stringValue = value.toString()

            if (preference is ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                val listPreference = preference
                val index = listPreference.findIndexOfValue(stringValue)

                // Set the summary to reflect the new value.
                preference.setSummary(
                    if (index >= 0)
                        listPreference.entries[index]
                    else
                        null
                )

            } else {
                preference.summary = stringValue
            }

            // Save preference value to shared preferences
            savePreferenceValueToSharedPrefs(preference, value)

            true
        }

        /**
         * Helper method to determine if the device has an extra-large screen. For
         * example, 10" tablets are extra-large.
         */
        private fun isXLargeTablet(context: Context): Boolean {
            return context.resources.configuration.screenLayout and Configuration.SCREENLAYOUT_SIZE_MASK >= Configuration.SCREENLAYOUT_SIZE_XLARGE
        }

        /**
         * Binds a preference's summary to its value. More specifically, when the
         * preference's value is changed, its summary (line of text below the
         * preference title) is updated to reflect the value. The summary is also
         * immediately updated upon calling this method. The exact display format is
         * dependent on the type of preference.

         * @see .sBindPreferenceSummaryToValueListenerAndSaveToSharedPrefs
         */
        private fun bindPreferenceSummaryToValue(preference: Preference) {
            // Set the listener to watch for value changes.
            preference.onPreferenceChangeListener =
                sBindPreferenceSummaryToValueListenerAndSaveToSharedPrefs

            // Trigger the listener immediately with the preference's
            // current value.
            sBindPreferenceSummaryToValueListenerAndSaveToSharedPrefs.onPreferenceChange(
                preference,
                PreferenceManager
                    .getDefaultSharedPreferences(preference.context)
                    .getString(preference.key, "")
            )
        }

        private fun handleCheckBoxPreferenceOnChangeListener(preference: Preference) {
            preference.setOnPreferenceChangeListener { _, value ->
                savePreferenceValueToSharedPrefs(preference, value)
                true
            }
        }

    }
}
