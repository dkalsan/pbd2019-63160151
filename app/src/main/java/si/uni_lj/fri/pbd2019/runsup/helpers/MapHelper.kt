package si.uni_lj.fri.pbd2019.runsup.helpers

import android.content.Context
import android.graphics.Color
import android.location.Location
import android.support.v4.content.ContextCompat
import android.view.View
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.google.firebase.database.*
import com.google.maps.android.ui.IconGenerator
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.R
import si.uni_lj.fri.pbd2019.runsup.model.Track
import si.uni_lj.fri.pbd2019.runsup.model.config.DatabaseHelper

class MapHelper {

    companion object {
        fun setUpMap(mMap: GoogleMap, workoutId: Long, context: Context, isFinished: Boolean) {

            val databaseHelper = DatabaseHelper(context)

            val workoutDao = databaseHelper.workoutDao()
            val workout = workoutDao.queryForId(workoutId)

            if(workout != null) {
                val gpsPointDao = databaseHelper.gpsPointDao()
                val qLocations = gpsPointDao.queryForEq("workout_id", workout)

                if(qLocations.isNotEmpty()) {

                    // Init icons
                    val icg = IconGenerator(context)
                    icg.setTextAppearance(R.style.TextAppearance_AppCompat_Button)

                    icg.setColor(Color.GREEN)
                    val startIcon = icg.makeIcon("Start")

                    icg.setColor(Color.RED)
                    val stopIcon = icg.makeIcon("Stop")

                    icg.setColor(Color.YELLOW)

                    val locationsBySession = qLocations.withIndex()
                        .groupBy { it.value.sessionNumber }
                        .map { it.value.map { entry -> LatLng(entry.value.latitude, entry.value.longitude) } }

                    // Set start icon
                    mMap.addMarker(
                        MarkerOptions()
                            .position(locationsBySession.first().first()))
                        .setIcon(BitmapDescriptorFactory.fromBitmap(startIcon))

                    // Set end icon

                    if(isFinished) {
                        mMap.addMarker(
                            MarkerOptions()
                                .position(locationsBySession.last().last()))
                            .setIcon(BitmapDescriptorFactory.fromBitmap(stopIcon))
                    }

                    locationsBySession.forEachIndexed { ix, _ ->
                        mMap.addPolyline(
                            PolylineOptions()
                                .addAll(
                                    ArrayList(locationsBySession[ix])
                                )
                        )

                        if(ix >= 1) {
                            val lastPoint = locationsBySession[ix-1].last()
                            val firstPoint = locationsBySession[ix].first()
                            var res = FloatArray(1)
                            Location.distanceBetween(lastPoint.latitude, lastPoint.longitude, firstPoint.latitude, firstPoint.longitude, res)

                            if(res[0] <= 100.0f) {
                                mMap.addPolyline(
                                    PolylineOptions()
                                        .add(firstPoint, lastPoint)
                                )

                                val breakIcon = icg.makeIcon("Break $ix")
                                mMap.addMarker(
                                    MarkerOptions()
                                        .position(lastPoint))
                                    .setIcon(BitmapDescriptorFactory.fromBitmap(breakIcon))

                            } else {
                                val pauseIcon = icg.makeIcon("Pause $ix")
                                val continueIcon = icg.makeIcon("Continue $ix")

                                mMap.addMarker(
                                    MarkerOptions()
                                        .position(lastPoint))
                                    .setIcon(BitmapDescriptorFactory.fromBitmap(pauseIcon))

                                mMap.addMarker(
                                    MarkerOptions()
                                        .position(firstPoint))
                                    .setIcon(BitmapDescriptorFactory.fromBitmap(continueIcon))
                            }
                        }
                    }

                    // Center view
                    val qLocationLatLng = qLocations.map { LatLng(it.latitude, it.longitude) }
                    val bounds = qLocationLatLng
                        .fold(LatLngBounds.Builder(), LatLngBounds.Builder::include)
                        .build()

                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 120))
                }
            }
        }

        // Function for setting up maps for tracks in compete module
        fun setUpMap(mMap: GoogleMap, trackId: String, context: Context) {

            // Init icons
            val icg = IconGenerator(context)
            icg.setTextAppearance(R.style.TextAppearance_AppCompat_Button)

            icg.setColor(Color.GREEN)
            val startIcon = icg.makeIcon("Start")

            icg.setColor(Color.RED)
            val endIcon = icg.makeIcon("End")

            // Get the track points
            val mDatabase = FirebaseDatabase.getInstance().reference
            mDatabase
                .child(Constants.FIREBASE_TRACKS_KEY)
                .child(trackId)
                .addValueEventListener(object: ValueEventListener {
                    override fun onCancelled(error: DatabaseError) = Unit
                    override fun onDataChange(snapshot: DataSnapshot) {
                        val track = snapshot.getValue(Track::class.java)

                        if(track != null) {
                            val geopoints = track.geopoints.map { LatLng(it.lat, it.lng) }

                            // Set start icon
                            mMap.addMarker(
                                MarkerOptions()
                                    .position(geopoints.first()))
                                .setIcon(BitmapDescriptorFactory.fromBitmap(startIcon))

                            // Set end icon

                            mMap.addMarker(
                                MarkerOptions()
                                    .position(geopoints.last()))
                                .setIcon(BitmapDescriptorFactory.fromBitmap(endIcon))

                            // Draw polyline
                            mMap.addPolyline(
                                PolylineOptions()
                                    .color(ContextCompat.getColor(context, R.color.colorPrimary))
                                    .addAll(
                                        ArrayList(geopoints)
                                    )
                            )

                            // Center view
                            val bounds = geopoints
                                .fold(LatLngBounds.Builder(), LatLngBounds.Builder::include)
                                .build()
                            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 120))

                        }
                    }
                })
        }
    }
}