package si.uni_lj.fri.pbd2019.runsup.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.IBinder
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.firebase.database.*
import si.uni_lj.fri.pbd2019.runsup.Constants
import si.uni_lj.fri.pbd2019.runsup.R
import si.uni_lj.fri.pbd2019.runsup.model.Track
import kotlin.math.asin
import kotlin.math.sin

class ChallengeService : Service() {

    companion object {
        private const val EARTH_RADIUS = 6378137
        private const val OFF_TRACK_NOTIFICATION_ID = 134213
        private const val SERVICE_NOTIFICATION_ID = 10
        private const val NOTIFICATION_CHANNEL = "Active challenge"
    }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mCompetitionRef: DatabaseReference
    private lateinit var track: Track
    private lateinit var identifier: String
    private lateinit var offtrackNotificationBuilder: NotificationCompat.Builder
    private lateinit var location: Location
    private var locationRequest: LocationRequest? = null
    private var started: Boolean = false
    private var finished: Boolean = false
    private var nextLocationIx: Int = 1
    private var userLost = false

    override fun onCreate() {
        super.onCreate()

        // Get the fused location provider
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Get database references
        mDatabase = FirebaseDatabase.getInstance().reference

        // Initialize location callback
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return

                location = locationResult.lastLocation

                // Post your current location
                if(::mCompetitionRef.isInitialized) {
                    mCompetitionRef
                        .child("competitor$identifier")
                        .child("lat")
                        .setValue(location.latitude)
                    mCompetitionRef
                        .child("competitor$identifier")
                        .child("lng")
                        .setValue(location.longitude)
                }

                if(started) {

                    // We arrived at the finish line
                    if(nextLocationIx == track.geopoints.size) {
                        // Push time to server
                        mCompetitionRef
                            .child("endTime$identifier")
                            .setValue(ServerValue.TIMESTAMP)

                        // Change flags
                        finished = true
                        started = false
                    } else {
                        doCalculations(location)
                    }
                }

                broadcastDetails(location, finished)
            }
        }

        // Initialize location request
        createLocationRequest()

        // Start the service in foreground and display notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Initialize channel
            val name = "Active channel"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(NOTIFICATION_CHANNEL, name, importance)
            mChannel.description = "Handles your location and notifies when you're off track."
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)

            // Start the service in foreground
            val serviceNotificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Location tracker")
                .setContentText("Just here to let you know if you get lost!")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            startForeground(SERVICE_NOTIFICATION_ID, serviceNotificationBuilder.build())
        }

        // Init offtrack notification builder
        offtrackNotificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("Location tracker")
            .setContentText("You're off track!")
            .setVibrate(longArrayOf(0, 500, 100, 500, 5000))
            .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
            .setPriority(NotificationCompat.PRIORITY_MAX)

        // Start location updates
        startLocationUpdates()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        when(intent?.action) {
            Constants.COMMAND_START -> {
                started = true
            }
            Constants.LOAD_TRACK_BY_ID -> {
                // Load track
                mDatabase
                    .child(Constants.FIREBASE_TRACKS_KEY)
                    .child(intent.getStringExtra("trackId"))
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onCancelled(error: DatabaseError) = Unit
                        override fun onDataChange(snapshot: DataSnapshot) {
                            track = snapshot.getValue(Track::class.java)!!
                        }
                    })

                // Save competition reference
                mCompetitionRef = mDatabase
                    .child(Constants.FIREBASE_COMPETITIONS_KEY)
                    .child(intent.getStringExtra("competitionId"))

                // Save identifier
                identifier = intent.getStringExtra("identifier")
            }
            Constants.COMMAND_STATUS -> {
                val bIntent = Intent(Constants.BROADCAST_CHALLENGE_STATUS).apply {
                    putExtra("finished", finished)
                }

                LocalBroadcastManager.getInstance(this).sendBroadcast(bIntent)
            }
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onDestroy() {
        stopLocationUpdates()
        with(NotificationManagerCompat.from(this)) {
            cancel(OFF_TRACK_NOTIFICATION_ID)
        }
        super.onDestroy()
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()?.apply {
            interval = 3000
            fastestInterval = 3000
            smallestDisplacement = 10f
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    private fun startLocationUpdates() {
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return
        }

        fusedLocationClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            null
        )
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private fun broadcastDetails(location: Location, finished: Boolean) {

        Log.d("RunsUp!", "\nSending broadcast:\nfinished: $finished\nlat:${location.latitude}\nlng:${location.longitude}")

        val intent = Intent(Constants.BROADCAST_CHALLENGE_DETAILS).apply {
            putExtra("location", location)
            putExtra("finished", finished)
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun doCalculations(location: Location) {

        // Check if user wandered off
        val isAwayFromPath = calculateIfUserWanderedOff(200.0, location)
        if(!userLost && isAwayFromPath) {
            Log.d("RunsUp!", "USER GOT LOST")
            userLost = true

            // Push notification
            with(NotificationManagerCompat.from(this)) {
                notify(OFF_TRACK_NOTIFICATION_ID, offtrackNotificationBuilder.build())
            }

        } else if(userLost && !isAwayFromPath) {
            Log.d("RunsUp!", "USER IS BACK ON TRACK")
            userLost = false

            // Cancel notification
            with(NotificationManagerCompat.from(this)) {
                cancel(OFF_TRACK_NOTIFICATION_ID)
            }
        }

        val nextLocation = Location(LocationManager.GPS_PROVIDER).apply {
            latitude = track.geopoints.elementAt(nextLocationIx).lat
            longitude = track.geopoints.elementAt(nextLocationIx).lng
        }

        if(location.distanceTo(nextLocation) < 30.0f) {
            nextLocationIx += 1
        }

    }

    private fun calculateIfUserWanderedOff(threshold: Double, currLocation: Location) : Boolean {
        val lastLocation = Location(LocationManager.GPS_PROVIDER).apply {
            latitude = track.geopoints.elementAt(nextLocationIx-1).lat
            longitude = track.geopoints.elementAt(nextLocationIx-1).lng
        }

        val nextLocation = Location(LocationManager.GPS_PROVIDER).apply {
            latitude = track.geopoints.elementAt(nextLocationIx).lat
            longitude = track.geopoints.elementAt(nextLocationIx).lng
        }

        // Calculate cross-track distance
        val delta13 = lastLocation.distanceTo(currLocation) / EARTH_RADIUS
        val theta13 = lastLocation.bearingTo(currLocation)
        val theta12 = lastLocation.bearingTo(nextLocation)

        val ctd = asin(sin(delta13) * sin(theta13-theta12)) * EARTH_RADIUS

        Log.d("RunsUp!", "CROSS DISTANCE: $ctd")

        return (ctd >= threshold || ctd <= -threshold)
    }

}