package si.uni_lj.fri.pbd2019.runsup

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.bumptech.glide.Glide
import si.uni_lj.fri.pbd2019.runsup.model.Workout
import kotlinx.android.synthetic.main.adapter_history.view.*
import si.uni_lj.fri.pbd2019.runsup.helpers.MainHelper
import java.text.DateFormat

class HistoryListAdapter(private val ctx: Context, private val resource: Int, private val workoutList: ArrayList<Workout>) : ArrayAdapter<Workout>(ctx, resource, workoutList)  {

    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun getCount(): Int {
        return workoutList.size
    }

    override fun getItem(position: Int): Workout {
        return workoutList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View
        val holder: ViewHolder

        if(convertView == null) {
            view = inflater.inflate(R.layout.adapter_history, parent, false)

            holder = ViewHolder()
            holder.titleTv = view.textview_history_title
            holder.datetimeTv = view.textview_history_datetime
            holder.summaryTv = view.textview_history_sportactivity
            holder.thumbnailIv = view.imageview_history_icon

            view.tag = holder
        } else {
            view = convertView
            holder = convertView.tag as ViewHolder
        }

        // Let the fragment handle the subitem click listeners
        view.imageview_history_icon.setOnClickListener {
            (parent as ListView).performItemClick(it, position, 0)
        }

        val workout = workoutList[position]

        // Get rid of potential null values
        workout.paceAvg = workout.paceAvg ?: 0.0
        workout.duration = workout.duration ?: 0L
        workout.sportActivity = workout.sportActivity ?: 0
        workout.totalCalories = workout.totalCalories ?: 0.0
        workout.distance = workout.distance ?: 0.0

        // Set title
        view.textview_history_title.text = workout.title

        // Set Date and time
        view.textview_history_datetime.text = DateFormat.getDateTimeInstance().format(workout.created)

        val unit = when (ctx.getSharedPreferences(Constants.DEFAULT_SHARED_PREFERENCES, Context.MODE_PRIVATE).getString(Constants.SAVED_UNIT_KEY, "0")) {
            "0" -> ctx.getString(R.string.all_labeldistanceunitkilometers)
            "1" -> ctx.getString(R.string.all_labeldistanceunitmiles)
            else -> "?"
        }

        // Set summary
        view.textview_history_sportactivity.text = ctx.getString(
            R.string.history_summary,
            MainHelper.formatDurationAux(workout.duration),
            view.resources.getStringArray(R.array.dialog_sport_list_values)[workout.sportActivity],
            MainHelper.formatDistance(if(unit == "mi") MainHelper.kmToMi(workout.distance) else workout.distance),
            unit,
            MainHelper.formatCalories(workout.totalCalories),
            ctx.getString(R.string.all_labelcaloriesunit),
            MainHelper.formatPace(if(unit == "mi") MainHelper.minpkmToMinpmi(workout.paceAvg) else workout.paceAvg),
            ctx.getString(R.string.all_min),
            unit
        )

        // Load image
        Glide
            .with(ctx)
            .load(workout.imageUrl)
            .asBitmap()
            .placeholder(R.drawable.ic_menu_camera)
            .into(view.imageview_history_icon)

        return view
    }

    private class ViewHolder {
        lateinit var titleTv: TextView
        lateinit var datetimeTv: TextView
        lateinit var summaryTv: TextView
        lateinit var thumbnailIv: ImageView
    }

}